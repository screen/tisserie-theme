var closeBtn = '',
    galleryPrev = '',
    galleryNext = '',
    windowWidth = 0,
    newPosition = '';

var swiper2 = new Swiper('.products-swiper', {
    slidesPerView: 1,
    spaceBetween: 0,
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev'
    }
});


var swiper3 = new Swiper('.about-testimonials-swiper', {
    slidesPerView: 1,
    spaceBetween: 0
});

var swiperHome = new Swiper('.main-home-slider-container', {
    slidesPerView: 1,
    spaceBetween: 0,
    autoplay: {
        delay: 8000,
        disableOnInteraction: false,
    },
    pagination: {
        el: '.swiper-pagination',
        type: 'bullets',
        clickable: true
    }
});

var swipeTestimonial = new Swiper('.testimonial-swiper-container', {
    slidesPerView: 2,
    loop: true,
    spaceBetween: 30,
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev'
    },
    breakpoints: {
        0: {
            slidesPerView: 1
        },
        991: {
            slidesPerView: 1
        },
        1024: {
            slidesPerView: 2
        }
    }
});


AOS.init({
    // Global settings:
    disable: false, // accepts following values: 'phone', 'tablet', 'mobile', boolean, expression or function
    startEvent: 'DOMContentLoaded', // name of the event dispatched on the document, that AOS should initialize on
    initClassName: 'aos-init', // class applied after initialization
    animatedClassName: 'aos-animate', // class applied on animation
    useClassNames: false, // if true, will add content of `data-aos` as classes on scroll
    disableMutationObserver: false, // disables automatic mutations' detections (advanced)
    debounceDelay: 50, // the delay on debounce used while resizing window (advanced)
    throttleDelay: 99, // the delay on throttle used while scrolling the page (advanced)


    // Settings that can be overridden on per-element basis, by `data-aos-*` attributes:
    offset: 120, // offset (in px) from the original trigger point
    delay: 0, // values from 0 to 3000, with step 50ms
    duration: 400, // values from 0 to 3000, with step 50ms
    easing: 'ease', // default easing for AOS animations
    once: false, // whether animation should happen only once - while scrolling down
    mirror: false, // whether elements should animate out while scrolling past them
    anchorPlacement: 'top-bottom', // defines which position of the element regarding to window should trigger the animation

});

function displayWindowSize() {
    windowWidth = window.innerWidth;
}

/* CUSTOM ON LOAD FUNCTIONS */
function documentCustomLoad() {
    "use strict";
    console.log('Functions Correctly Loaded');
    windowWidth = window.innerWidth;
    window.addEventListener("resize", displayWindowSize);

    // the current open accordion will not be able to close itself
    $('[data-toggle="collapse"]').on('click', function(e) {
        if (windowWidth < 768) {
            if ($(this).parents('.custom-accordion').find('.collapse .show')) {
                var idx = $(this).index('[data-toggle="collapse"]');
                if (idx == $('.collapse .show').index('.collapse')) {
                    e.stopPropagation();
                }
            }
        }
    });

    var productWrapper = document.getElementsByClassName('product-image-wrapper');
    if (productWrapper[0] != null) {
        for (var i = 0; i < productWrapper.length; i++) {
            productWrapper[i].addEventListener('click', openOverlay, false);
        }
    }

    document.addEventListener("scroll", (event) => {
        var headerSticky = document.getElementsByClassName('the-header');
        var scrollTop = window.pageYOffset || document.documentElement.scrollTop;
        if (scrollTop > 60) {
            headerSticky[0].classList.add('header-visible');
        } else {
            headerSticky[0].classList.remove('header-visible');
        }
    });

    var menuBtn = document.getElementsByClassName('menu-opener');
    menuBtn[0].addEventListener('click', function(e) {
        e.preventDefault();
        var scrollTop = window.pageYOffset || document.documentElement.scrollTop;
        var headerSticky = document.getElementsByClassName('the-header');
        var menuMobile = document.getElementsByClassName('header-menu-mobile');

        if (scrollTop > 60) {
            menuMobile[0].classList.toggle('menu-mobile-shown');
        } else {
            menuMobile[0].classList.toggle('menu-mobile-shown');
            headerSticky[0].classList.toggle('header-visible');
        }
    });

    jQuery('.btn-collapse').on('click', function(e) {
        var target = jQuery(this).data('target');
        var targetNext = '';
        if (windowWidth < 768) {
            $(".btn-collapse").each(function() {
                targetNext = jQuery(this).data('target');
                if (targetNext != target) {
                    jQuery(this).removeClass('collapsed');
                }
            });
            $(".collapse").each(function() {
                jQuery(this).removeClass('show');
            });

        }
        if (jQuery(this).hasClass('collapsed')) {
            jQuery(this).addClass('collapsing');

            jQuery(target).removeClass('show');
            jQuery(this).removeClass('collapsing');
            jQuery(this).removeClass('collapsed');

        } else {
            jQuery(this).addClass('collapsed');
            jQuery(this).addClass('collapsing');
            jQuery(target).addClass('show');
            jQuery(this).removeClass('collapsing');
            jQuery(this).addClass('collapsed');

        }
        if (windowWidth < 768) {
            var $card = jQuery(this).closest('.card');
            jQuery('html,body').animate({
                scrollTop: $card.offset().top - 100
            }, 30);
        }
    });


    jQuery('.collapse').on('shown.bs.collapse', function(e) {
        if (windowWidth < 767) {
            var $card = jQuery(this).closest('.card');
            jQuery('html,body').animate({
                scrollTop: $card.offset().top - 100
            }, 30);
        }
    });

}

document.addEventListener("DOMContentLoaded", documentCustomLoad, false);

function openOverlay() {
    var imgID = this.dataset.img;
    var position = this.dataset.number;
    var postID = this.id;
    var mainOverlay = document.getElementById('overlay');
    if (mainOverlay == null) {
        mainOverlay = document.createElement('div');
        mainOverlay.setAttribute("id", "overlay");
        document.body.appendChild(mainOverlay);
    } else {
        mainOverlay.innerHTML = '';
        mainOverlay.classList.remove('d-none');
    }



    var loaderCSS = document.createElement('div');
    loaderCSS.setAttribute("class", "loader-css");

    mainOverlay.appendChild(loaderCSS);

    var info = 'action=gallery_wrapper&imgID=' + imgID + '&position=' + position + '&postID=' + postID;
    /* SEND AJAX */
    newRequest = new XMLHttpRequest();
    newRequest.open('POST', custom_admin_url.ajax_url, true);
    newRequest.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    newRequest.onload = function() {
        var result = JSON.parse(newRequest.responseText);
        if (result.success == true) {
            mainOverlay.innerHTML = '';
            mainOverlay.innerHTML = result.data;

            closeBtn = document.getElementsByClassName('gallery-close');
            closeBtn[0].addEventListener('click', function() {
                mainOverlay.classList.add('d-none');
            }, true);

            galleryPrev = document.getElementsByClassName('gallery-prev');
            galleryPrev[0].addEventListener('click', galleryChanger, true);


            galleryNext = document.getElementsByClassName('gallery-next');
            galleryNext[0].addEventListener('click', galleryChanger, true);

        } else {
            swal({
                title: 'Error on Request',
                text: "There's an error in the request",
                icon: 'error',
                buttons: {
                    confirm: {
                        className: 'btn btn-danger',
                    },
                },
            });
        }
    };
    newRequest.send(info);
}

function galleryChanger() {
    var mainOverlay = document.getElementById('overlay');
    var productWrapper = document.getElementsByClassName('product-image-wrapper');
    var maximumPos = productWrapper.length;
    var currentPos = this.dataset.position;
    if (currentPos < 1) {
        newPosition = maximumPos;
    } else if (currentPos > maximumPos) {
        newPosition = 1;
    } else {
        newPosition = currentPos;
    }

    var queryWrapper = document.querySelectorAll('[data-number="' + newPosition + '"]');

    var imgID = queryWrapper[0].dataset.img;
    var position = queryWrapper[0].dataset.number;
    var postID = queryWrapper[0].id;
    mainOverlay.innerHTML = '';
    mainOverlay.classList.remove('d-none');

    var loaderCSS = document.createElement('div');
    loaderCSS.setAttribute("class", "loader-css");

    mainOverlay.appendChild(loaderCSS);

    var info = 'action=gallery_wrapper&imgID=' + imgID + '&position=' + position + '&postID=' + postID;
    /* SEND AJAX */
    newRequest = new XMLHttpRequest();
    newRequest.open('POST', custom_admin_url.ajax_url, true);
    newRequest.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    newRequest.onload = function() {
        var result = JSON.parse(newRequest.responseText);
        if (result.success == true) {
            mainOverlay.innerHTML = '';
            mainOverlay.innerHTML = result.data;

            closeBtn = document.getElementsByClassName('gallery-close');
            closeBtn[0].addEventListener('click', function() {
                mainOverlay.classList.add('d-none');
            }, true);

            galleryPrev = document.getElementsByClassName('gallery-prev');
            galleryPrev[0].addEventListener('click', galleryChanger, true);


            galleryNext = document.getElementsByClassName('gallery-next');
            galleryNext[0].addEventListener('click', galleryChanger, true);

        } else {
            swal({
                title: 'Error on Request',
                text: "There's an error in the request",
                icon: 'error',
                buttons: {
                    confirm: {
                        className: 'btn btn-danger',
                    },
                },
            });
        }
    };
    newRequest.send(info);


}