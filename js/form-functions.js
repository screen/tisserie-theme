var passd = true;
var formModal = document.getElementById('formModal');
var formContact = document.getElementById('formContact');

function validateEmail(email) {
    var re = /\S+@\S+\.\S+/;
    return re.test(email);
}

function validateZipCode(elementValue) {
    var zipCodePattern = /^\d{5}$|^\d{5}-\d{4}$/;
    return zipCodePattern.test(elementValue);
}

function telephoneCheck(str) {
    var patt = new RegExp(/^(\+{0,})(\d{0,})([(]{1}\d{1,3}[)]{0,}){0,}(\s?\d+|\+\d{2,3}\s{1}\d+|\d+){1}[\s|-]?\d+([\s|-]?\d+){1,2}(\s){0,}$/gm);
    return patt.test(str);
}

if (formModal != null) {
    formModal.addEventListener('submit', function (e) {
        e.preventDefault();
        passd = true;

        /* NAME VALIDATION  */
        var inputText = document.getElementById('modal-name').value;
        var errorLabel = document.getElementsByClassName('alert-name');
        if (inputText == '' || inputText == null) {
            if (errorLabel[0].classList.contains('d-none')) {
                errorLabel[0].classList.toggle("d-none");
            }
            errorLabel[0].innerHTML = custom_admin_url.error_nombre;
            passd = false;
        } else {
            if (inputText.length < 3) {
                if (errorLabel[0].classList.contains('d-none')) {
                    errorLabel[0].classList.toggle("d-none");
                }
                elements[0].innerHTML = custom_admin_url.invalid_nombre;
                passd = false;
            } else {
                if (errorLabel[0].classList.contains('d-none')) {
                    errorLabel[0].classList.toggle("d-none");
                }
                errorLabel[0].innerHTML = '';
            }
        }

        /* EMAIL VALIDATION  */
        var inputText = document.getElementById('modal-email').value;
        var errorLabel = document.getElementsByClassName('alert-email');
        if (inputText == '' || inputText == null) {
            if (errorLabel[0].classList.contains('d-none')) {
                errorLabel[0].classList.toggle("d-none");
            }
            errorLabel[0].innerHTML = custom_admin_url.error_email;
            passd = false;
        } else {
            if (validateEmail(inputText) == false) {
                if (errorLabel[0].classList.contains('d-none')) {
                    errorLabel[0].classList.toggle("d-none");
                }
                errorLabel[0].innerHTML = custom_admin_url.invalid_email;
                passd = false;
            } else {
                if (errorLabel[0].classList.contains('d-none')) {
                    errorLabel[0].classList.toggle("d-none");
                }
                errorLabel[0].innerHTML = '';
            }
        }

        /* PHONE VALIDATION  */
        var inputText = document.getElementById('modal-phone').value;
        var errorLabel = document.getElementsByClassName('alert-phone');
        if (inputText == '' || inputText == null) {
            if (errorLabel[0].classList.contains('d-none')) {
                errorLabel[0].classList.toggle("d-none");
            }
            errorLabel[0].innerHTML = custom_admin_url.error_phone;
            passd = false;
        } else {
            if (telephoneCheck(inputText) == false) {
                if (errorLabel[0].classList.contains('d-none')) {
                    errorLabel[0].classList.toggle("d-none");
                }
                errorLabel[0].innerHTML = custom_admin_url.invalid_phone;
                passd = false;
            } else {
                if (errorLabel[0].classList.contains('d-none')) {
                    errorLabel[0].classList.toggle("d-none");
                }
                errorLabel[0].innerHTML = '';
            }
        }

        /* ADDRESS TYPE VALIDATION  */
        var inputText = document.getElementById('modal-address');
        var selectValue = inputText.options[inputText.selectedIndex].value;
        var errorLabel = document.getElementsByClassName('alert-address');
        if (selectValue == '') {
            if (errorLabel[0].classList.contains('d-none')) {
                errorLabel[0].classList.toggle("d-none");
            }
            errorLabel[0].innerHTML = custom_admin_url.error_address;
            passd = false;
        } else {
            if (errorLabel[0].classList.contains('d-none')) {
                errorLabel[0].classList.toggle("d-none");
            }
            errorLabel[0].innerHTML = '';
        }

        /* ZIPCODE VALIDATION  */
        var inputText = document.getElementById('modal-zipcode').value;
        var errorLabel = document.getElementsByClassName('alert-zipcode');
        if (inputText == '' || inputText == null) {
            if (errorLabel[0].classList.contains('d-none')) {
                errorLabel[0].classList.toggle("d-none");
            }
            errorLabel[0].innerHTML = custom_admin_url.error_zipcode;
            passd = false;
        } else {
            if (validateZipCode(inputText) == false) {
                if (errorLabel[0].classList.contains('d-none')) {
                    errorLabel[0].classList.toggle("d-none");
                }
                errorLabel[0].innerHTML = custom_admin_url.invalid_zipcode;
                passd = false;
            } else {
                if (errorLabel[0].classList.contains('d-none')) {
                    errorLabel[0].classList.toggle("d-none");
                }
                errorLabel[0].innerHTML = '';
            }
        }

        /* ROLE VALIDATION  */
        var inputText = document.getElementById('modal-role').value;
        var errorLabel = document.getElementsByClassName('alert-role');
        if (inputText == '' || inputText < 0) {
            if (errorLabel[0].classList.contains('d-none')) {
                errorLabel[0].classList.toggle("d-none");
            }
            errorLabel[0].innerHTML = custom_admin_url.error_role;
            passd = false;
        } else {
            if (errorLabel[0].classList.contains('d-none')) {
                errorLabel[0].classList.toggle("d-none");
            }
            errorLabel[0].innerHTML = '';
        }

        /* COMPANY NAME VALIDATION  */
        var inputText = document.getElementById('modal-company-name').value;
        var errorLabel = document.getElementsByClassName('alert-company-name');
        if (inputText == '' || inputText < 0) {
            if (errorLabel[0].classList.contains('d-none')) {
                errorLabel[0].classList.toggle("d-none");
            }
            errorLabel[0].innerHTML = custom_admin_url.error_company_name;
            passd = false;
        } else {
            if (errorLabel[0].classList.contains('d-none')) {
                errorLabel[0].classList.toggle("d-none");
            }
            errorLabel[0].innerHTML = '';
        }

        /* COMPANY NAME VALIDATION  */
        var inputText = document.getElementById('modal-company-size').value;
        var errorLabel = document.getElementsByClassName('alert-company-size');
        if (inputText == '' || inputText < 0) {
            if (errorLabel[0].classList.contains('d-none')) {
                errorLabel[0].classList.toggle("d-none");
            }
            errorLabel[0].innerHTML = custom_admin_url.error_company_size;
            passd = false;
        } else {
            if (errorLabel[0].classList.contains('d-none')) {
                errorLabel[0].classList.toggle("d-none");
            }
            errorLabel[0].innerHTML = '';
        }

        /* BUSINESS TYPE VALIDATION  */
        var inputText = document.getElementById('modal-business-type');
        var selectValue = inputText.options[inputText.selectedIndex].value;
        var errorLabel = document.getElementsByClassName('alert-business-type');
        if (selectValue == '') {
            if (errorLabel[0].classList.contains('d-none')) {
                errorLabel[0].classList.toggle("d-none");
            }
            errorLabel[0].innerHTML = custom_admin_url.error_business_type;
            passd = false;
        } else {
            if (errorLabel[0].classList.contains('d-none')) {
                errorLabel[0].classList.toggle("d-none");
            }
            errorLabel[0].innerHTML = '';
        }

        /* FREQUENCY OF THE DELIVER VALIDATION  */
        var inputText = document.getElementById('modal-frequency');
        var selectValue = inputText.options[inputText.selectedIndex].value;
        var errorLabel = document.getElementsByClassName('alert-frequency');
        if (selectValue == '') {
            if (errorLabel[0].classList.contains('d-none')) {
                errorLabel[0].classList.toggle("d-none");
            }
            errorLabel[0].innerHTML = custom_admin_url.error_frequency;
            passd = false;
        } else {
            if (errorLabel[0].classList.contains('d-none')) {
                errorLabel[0].classList.toggle("d-none");
            }
            errorLabel[0].innerHTML = '';
        }

        if (passd == true) {
            var info = 'action=send_message&modal-name=' +
                document.getElementById('modal-name').value + '&modal-email=' + document.getElementById('modal-email').value + '&modal-phone=' + document.getElementById('modal-phone').value + '&modal-address=' + document.getElementById('modal-address').value + '&modal-zipcode=' + document.getElementById('modal-zipcode').value + '&modal-role=' + document.getElementById('modal-role').value + '&modal-company-name=' + document.getElementById('modal-company-name').value + '&modal-company-size=' + document.getElementById('modal-company-size').value + '&modal-business-type=' + document.getElementById('modal-business-type').value + '&modal-frequency=' + document.getElementById('modal-frequency').value;
            var elements = document.getElementsByClassName('loader-css');
            elements[0].classList.toggle("d-none");
            /* SEND AJAX */
            newRequest = new XMLHttpRequest();
            newRequest.open('POST', custom_admin_url.ajax_url, true);
            newRequest.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
            newRequest.onload = function () {
                var result = JSON.parse(newRequest.responseText);
                if (result.success == true) {
                    elements[0].classList.toggle("d-none");
                    window.location.href = custom_admin_url.thanks_url;
                } else {
                    elements[0].classList.toggle("d-none");
                    swal({
                        title: 'Error on Request',
                        text: custom_admin_url.error_form,
                        icon: 'error',
                        buttons: {
                            confirm: {
                                className: 'btn btn-danger',
                            },
                        },
                    });
                }
            };
            newRequest.send(info);
        }

    }, true);
}

if (formContact != null) {
    formContact.addEventListener('submit', function (e) {
        e.preventDefault();
        passd = true;

        /* NAME VALIDATION  */
        var inputText = document.getElementById('contact-name').value;
        var errorLabel = document.getElementsByClassName('alert-name');
        if (inputText == '' || inputText == null) {
            if (errorLabel[0].classList.contains('d-none')) {
                errorLabel[0].classList.toggle("d-none");
            }
            errorLabel[0].innerHTML = custom_admin_url.error_nombre;
            passd = false;
        } else {
            if (inputText.length < 3) {
                if (errorLabel[0].classList.contains('d-none')) {
                    errorLabel[0].classList.toggle("d-none");
                }
                elements[0].innerHTML = custom_admin_url.invalid_nombre;
                passd = false;
            } else {
                if (errorLabel[0].classList.contains('d-none')) {
                    errorLabel[0].classList.toggle("d-none");
                }
                errorLabel[0].innerHTML = '';
            }
        }

        /* EMAIL VALIDATION  */
        var inputText = document.getElementById('contact-email').value;
        var errorLabel = document.getElementsByClassName('alert-email');
        if (inputText == '' || inputText == null) {
            if (errorLabel[0].classList.contains('d-none')) {
                errorLabel[0].classList.toggle("d-none");
            }
            errorLabel[0].innerHTML = custom_admin_url.error_email;
            passd = false;
        } else {
            if (validateEmail(inputText) == false) {
                if (errorLabel[0].classList.contains('d-none')) {
                    errorLabel[0].classList.toggle("d-none");
                }
                errorLabel[0].innerHTML = custom_admin_url.invalid_email;
                passd = false;
            } else {
                if (errorLabel[0].classList.contains('d-none')) {
                    errorLabel[0].classList.toggle("d-none");
                }
                errorLabel[0].innerHTML = '';
            }
        }

        /* PHONE VALIDATION  */
        var inputText = document.getElementById('contact-phone').value;
        var errorLabel = document.getElementsByClassName('alert-phone');
        if (inputText == '' || inputText == null) {
            if (errorLabel[0].classList.contains('d-none')) {
                errorLabel[0].classList.toggle("d-none");
            }
            errorLabel[0].innerHTML = custom_admin_url.error_phone;
            passd = false;
        } else {
            if (telephoneCheck(inputText) == false) {
                if (errorLabel[0].classList.contains('d-none')) {
                    errorLabel[0].classList.toggle("d-none");
                }
                errorLabel[0].innerHTML = custom_admin_url.invalid_phone;
                passd = false;
            } else {
                if (errorLabel[0].classList.contains('d-none')) {
                    errorLabel[0].classList.toggle("d-none");
                }
                errorLabel[0].innerHTML = '';
            }
        }
        
        /* COMPANY NAME VALIDATION  */
        var inputText = document.getElementById('contact-company-name').value;
        var errorLabel = document.getElementsByClassName('alert-company-name');
        if (inputText == '' || inputText < 0) {
            if (errorLabel[0].classList.contains('d-none')) {
                errorLabel[0].classList.toggle("d-none");
            }
            errorLabel[0].innerHTML = custom_admin_url.error_company_name;
            passd = false;
        } else {
            if (errorLabel[0].classList.contains('d-none')) {
                errorLabel[0].classList.toggle("d-none");
            }
            errorLabel[0].innerHTML = '';
        }

        /* COMPANY NAME VALIDATION  */
        var inputText = document.getElementById('contact-subject').value;
        var errorLabel = document.getElementsByClassName('alert-subject');
        if (inputText == '' || inputText < 0) {
            if (errorLabel[0].classList.contains('d-none')) {
                errorLabel[0].classList.toggle("d-none");
            }
            errorLabel[0].innerHTML = custom_admin_url.error_subject;
            passd = false;
        } else {
            if (errorLabel[0].classList.contains('d-none')) {
                errorLabel[0].classList.toggle("d-none");
            }
            errorLabel[0].innerHTML = '';
        }

        /* BUSINESS TYPE VALIDATION  */
        var inputText = document.getElementById('contact-message').value;
        var errorLabel = document.getElementsByClassName('alert-message');
        if (inputText == '') {
            if (errorLabel[0].classList.contains('d-none')) {
                errorLabel[0].classList.toggle("d-none");
            }
            errorLabel[0].innerHTML = custom_admin_url.error_message;
            passd = false;
        } else {
            if (errorLabel[0].classList.contains('d-none')) {
                errorLabel[0].classList.toggle("d-none");
            }
            errorLabel[0].innerHTML = '';
        }

        if (passd == true) {
            var info = 'action=send_contact&modal-name=' +
                document.getElementById('contact-name').value + '&contact-email=' + document.getElementById('contact-email').value + '&contact-phone=' + document.getElementById('contact-phone').value + '&contact-company-name=' + document.getElementById('contact-company-name').value + '&contact-subject=' + document.getElementById('contact-subject').value + '&contact-message=' + document.getElementById('contact-message').value;
            var elements = document.getElementsByClassName('loader-css');
            elements[0].classList.toggle("d-none");
            /* SEND AJAX */
            newRequest = new XMLHttpRequest();
            newRequest.open('POST', custom_admin_url.ajax_url, true);
            newRequest.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
            newRequest.onload = function () {
                var result = JSON.parse(newRequest.responseText);
                if (result.success == true) {
                    elements[0].classList.toggle("d-none");
                    window.location.href = custom_admin_url.thanks_url;
                } else {
                    window.location.href = custom_admin_url.thanks_url;
                }
            };
            newRequest.send(info);
        }

    }, true);
}