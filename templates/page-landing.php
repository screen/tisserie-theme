<?php
/**
* Template Name: Landing Home
*
* @package tisserie
* @subpackage tisserie-mk01-theme
* @since Mk. 1.0
*/
?>
<?php get_header('landing'); ?>
<?php the_post(); ?>
<main class="container-fluid p-0" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row no-gutters">
        <?php $bg_banner_id = get_post_meta(get_the_ID(), 'tsr_hero_banner_bg_id', true); ?>
        <?php $bg_banner = wp_get_attachment_image_src($bg_banner_id, 'full', false); ?>
        <section class="main-hero-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" style="background: url(<?php echo $bg_banner[0]; ?>);">
            <div class="container">
                <div class="row align-items-center">
                    <div class="main-hero-content col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" data-aos="fade" data-aos-delay="150">
                        <?php $bg_banner_id = get_post_meta(get_the_ID(), 'tsr_hero_banner_logo_id', true); ?>
                        <?php $bg_banner = wp_get_attachment_image_src($bg_banner_id, 'full', false); ?>
                        <img itemprop="image" content="<?php echo $bg_banner[0];?>" src="<?php echo $bg_banner[0];?>" title="<?php echo get_post_meta( $bg_banner_id, '_wp_attachment_image_alt', true ); ?>" alt="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true ); ?>" class="img-fluid" width="<?php echo $bg_banner[1]; ?>" height="<?php echo $bg_banner[2]; ?>" />
                        <?php the_content(); ?>
                        <?php $modal = get_post_meta(get_the_ID(), 'tsr_hero_banner_button_modal', true); ?>
                        <?php $button_link = get_post_meta(get_the_ID(), 'tsr_hero_banner_button_url', true); ?>
                        <?php if ($modal == 'on') { $elm = 'href="#"  data-toggle="modal" data-target="#contactModal"'; } else { $elm = 'href="'. $button_link . '"'; }?>
                        <a <?php echo $elm; ?> class="btn btn-md btn-yellow"><?php echo get_post_meta(get_the_ID(), 'tsr_hero_banner_button_text', true);?></a>
                    </div>
                </div>
            </div>
        </section>
        <?php $bg_banner_id = get_post_meta(get_the_ID(), 'tsr_hero_column2_bg_id', true); ?>
        <?php $bg_banner = wp_get_attachment_image_src($bg_banner_id, 'full', false); ?>
        <section class="two-columns-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" style="background: url(<?php echo $bg_banner[0]; ?>);">
            <div class="container">
                <div class="row">
                    <div class="two-column-item column1 col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        <div class="two-columns-item-wrapper">
                            <?php $bg_banner_id = get_post_meta(get_the_ID(), 'tsr_hero_column1_bg_id', true); ?>
                            <?php $bg_banner = wp_get_attachment_image_src($bg_banner_id, 'full', false); ?>
                            <img itemprop="image" content="<?php echo $bg_banner[0];?>" src="<?php echo $bg_banner[0];?>" title="<?php echo get_post_meta( $bg_banner_id, '_wp_attachment_image_alt', true ); ?>" alt="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true ); ?>" class="img-fluid" width="<?php echo $bg_banner[1]; ?>" height="<?php echo $bg_banner[2]; ?>" data-aos="fade" data-aos-delay="150" />
                            <div class="two-items-wrapper" data-aos="fade" data-aos-delay="250">
                                <h2><?php echo get_post_meta(get_the_ID(), 'tsr_hero_column1_text', true); ?></h2>
                            </div>
                        </div>
                    </div>

                    <?php $bg_banner_id = get_post_meta(get_the_ID(), 'tsr_hero_column2_bg_id', true); ?>
                    <?php $bg_banner = wp_get_attachment_image_src($bg_banner_id, 'full', false); ?>
                    <div class="two-column-item column2 col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12" data-aos="fade" data-aos-delay="150">
                        <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'tsr_hero_column2_text', true)); ?>
                        <div class="row">
                            <div class="column2-subitem col-6" data-aos="fade" data-aos-delay="250">
                                <h3>24/7</h3>
                                <img src="<?php echo get_template_directory_uri(); ?>/images/separator-yellow.png" alt="separator" class="img-fluid">
                                <h4><?php _e('Service', 'tisserie'); ?></h4>
                            </div>
                            <div class="column2-subitem col-6" data-aos="fade" data-aos-delay="250">
                                <h3>365</h3>
                                <img src="<?php echo get_template_directory_uri(); ?>/images/separator-yellow.png" alt="separator" class="img-fluid">
                                <h4><?php _e('Days of the Year', 'tisserie'); ?></h4>
                            </div>
                            <div class="column2-subitem col-12" data-aos="fade" data-aos-delay="350">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/delivery.png" alt="" class="img-fluid">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="subscribe-bar-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row align-items-center">
                    <div class="subscribe-bar-content col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12" data-aos="fade" data-aos-delay="150">
                        <div class="custom-media media">
                            <?php $bg_banner_id = get_post_meta(get_the_ID(), 'tsr_home_subscription_icon_id', true); ?>
                            <?php $bg_banner = wp_get_attachment_image_src($bg_banner_id, 'full', false); ?>
                            <img src="<?php echo $bg_banner[0];?>" title="<?php echo get_post_meta( $bg_banner_id, '_wp_attachment_image_alt', true ); ?>" alt="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true ); ?>" class="align-self-center mr-3" width="<?php echo $bg_banner[1]; ?>" height="<?php echo $bg_banner[2]; ?>" />
                            <div class="media-body">
                                <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'tsr_home_subscription_text', true)); ?>
                            </div>
                        </div>

                    </div>
                    <div class="subscribe-bar-form col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12" data-aos="fade" data-aos-delay="250">
                        <?php echo get_template_part('templates/template-mailchimp-form'); ?>
                    </div>
                </div>
            </div>
        </section>
        <section class="about-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row">
                    <div class="about-column-item column1 col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 order-xl-1 order-lg-1 order-md-1 order-sm-12 order-12" data-aos="fade" data-aos-delay="150">
                        <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'tsr_home_about_main_text', true)); ?>
                        <img src="<?php echo get_template_directory_uri(); ?>/images/separator-crown.png" alt="Crown" class="img-fluid img-separator" />
                        <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'tsr_home_about_second_text', true)); ?>
                        <?php $arr_gallery = get_post_meta(get_the_ID(), 'tsr_home_about_icon', true); ?>
                        <?php foreach ($arr_gallery as $key => $value) { ?>
                        <?php $bg_banner = wp_get_attachment_image_src($key, 'full', false); ?>
                        <img itemprop="image" content="<?php echo $bg_banner[0];?>" src="<?php echo $bg_banner[0];?>" title="<?php echo get_post_meta( $key, '_wp_attachment_image_alt', true ); ?>" alt="<?php echo get_post_meta($key, '_wp_attachment_image_alt', true ); ?>" class="img-fluid img-final-icon" width="<?php echo $bg_banner[1]; ?>" height="<?php echo $bg_banner[2]; ?>" />
                        <?php } ?>
                    </div>

                    <div class="about-column-item column2 col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 order-xl-12 order-lg-12 order-md-12 order-sm-1 order-1">
                        <?php $bg_banner_id = get_post_meta(get_the_ID(), 'tsr_home_about_image_id', true); ?>
                        <?php $bg_banner = wp_get_attachment_image_src($bg_banner_id, 'full', false); ?>
                        <img itemprop="image" content="<?php echo $bg_banner[0];?>" src="<?php echo $bg_banner[0];?>" title="<?php echo get_post_meta( $bg_banner_id, '_wp_attachment_image_alt', true ); ?>" alt="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true ); ?>" class="img-fluid" width="<?php echo $bg_banner[1]; ?>" height="<?php echo $bg_banner[2]; ?>" data-aos="fade" data-aos-delay="200" />
                    </div>
                </div>
            </div>
        </section>
        <section class="products-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row">
                    <div class="products-content col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <h2><?php echo get_post_meta(get_the_ID(), 'tsr_home_products_title', true); ?></h2>
                    </div>

                    <?php $args = array('post_type' => 'product', 'posts_per_page' => -1, 'order' => 'ASC', 'orderby' => 'date'); ?>
                    <?php $arr_products = new WP_Query($args); ?>

                    <?php if ($arr_products->have_posts()) : ?>
                    <div class="container">
                        <div class="row">
                            <?php $i = 1; $y = 1; ?>
                            <?php while ($arr_products->have_posts()) : $arr_products->the_post(); ?>
                            <?php $delay = 50 * $y; ?>
                            <div class="product-item col-xl-3 col-lg-3 col-md-3 col-sm-6 col-6" data-aos="fade" data-aos-delay="<?php echo $delay; ?>">
                                <div class="product-item-image">
                                    <?php the_post_thumbnail('catalog_img', array('class' => 'img-fluid')); ?>
                                    <div id="<?php echo get_the_ID(); ?>" data-number="<?php echo $i; ?>" data-img="<?php echo get_post_thumbnail_id(); ?>" class="product-image-wrapper">
                                        <img src="<?php echo get_template_directory_uri(); ?>/images/logo-t.png" alt="Logo" class="img-fluid img-logo-wrapper" />
                                    </div>
                                </div>
                                <h3><?php the_title(); ?></h3>
                            </div>
                            <?php if ($y == 8) { $y = 1; } ?>
                            <?php $i++; $y++; endwhile; ?>
                        </div>
                    </div>
                    <?php endif; ?>
                    <?php wp_reset_query(); ?>
                </div>
            </div>
        </section>
        <section class="testimonials-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row">
                    <div class="testimonials-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" data-aos="fade" data-aos-delay="200">
                        <div class="testimonials-main-title">
                            <h2><?php echo get_post_meta(get_the_ID(), 'tsr_home_testimonials_title', true); ?></h2>
                        </div>

                        <div class="testimonials-main-slider-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" data-aos="fade" data-aos-delay="200">
                            <div class="row">
                                <?php $arr_testimonials = get_post_meta(get_the_ID(), 'tsr_home_testimonials_group', true); ?>
                                <?php if (!empty($arr_testimonials)) : ?>

                                <?php $i = 1; ?>
                                <?php foreach ($arr_testimonials as $item) { ?>
                                <div class="testimonial-item col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12">
                                    <div class="swiper-testimonial-wrapper">
                                        <div class="swiper-testimonial-hidden">
                                            <div class="swiper-front-wrapper">
                                                <?php $bg_banner = wp_get_attachment_image_src($item['logo_id'], 'full', false); ?>
                                                <img itemprop="image" content="<?php echo $bg_banner[0];?>" src="<?php echo $bg_banner[0];?>" title="<?php echo get_post_meta( $bg_banner_id, '_wp_attachment_image_alt', true ); ?>" alt="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true ); ?>" class="img-fluid" width="<?php echo $bg_banner[1]; ?>" height="<?php echo $bg_banner[2]; ?>" />
                                                <div class="star-container">
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                </div>
                                                <a href="#" data-wrapper="<?php echo $i; ?>" class="testimonial-opener"><i class="fa fa-chevron-up"></i><?php _e('Learn More', 'tisserie'); ?></a>
                                                <div id="wrapper_<?php echo $i; ?>" class="swiper-back-wrapper">
                                                    <?php echo apply_filters('the_content', $item['desc']); ?>
                                                    <h5><?php echo $item['author']; ?></h5>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <?php $i++; } ?>

                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php $arr_gallery = get_post_meta(get_the_ID(), 'tsr_home_gallery_list', true); ?>
        <?php $i = 1; ?>
        <?php if (!empty($arr_gallery)) : ?>
        <section class="gallery-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <img src="<?php echo get_template_directory_uri(); ?>/images/support.png" alt="Support local Vendors" class="img-fluid img-support" />
            <div class="container-fluid p-0">
                <div class="row no-gutters minimal">
                    <?php $i = 1; ?>
                    <?php foreach ($arr_gallery as $key => $value) { ?>
                    <?php if ($i == 1 || $i == 3 || $i == 6) { ?>
                    <section class="gallery-columns col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12">
                        <?php } ?>

                        <?php $size = ($i == 2 || $i == 6) ? 'special_large' : 'special_normal'; ?>
                        <?php if ($i == 3) { $size = 'special_medium'; } ?>
                        <?php $delay = 50 * $i; ?>
                        <div class="gallery-item <?php echo $size; ?>" data-aos="fade" data-aos-delay="<?php echo $delay; ?>">
                            <?php $bg_banner = wp_get_attachment_image_src($key, $size, false); ?>
                            <img itemprop="image" content="<?php echo $bg_banner[0];?>" src="<?php echo $bg_banner[0];?>" title="<?php echo get_post_meta( $bg_banner_id, '_wp_attachment_image_alt', true ); ?>" alt="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true ); ?>" class="img-fluid" width="<?php echo $bg_banner[1]; ?>" height="<?php echo $bg_banner[2]; ?>" />
                        </div>

                        <?php if ($i == 2 || $i == 5 || $i == 8) { ?>
                    </section>
                    <?php } ?>
                    <?php $i++; } ?>
                </div>
            </div>
        </section>
        <?php endif; ?>
    </div>
</main>
<!-- Modal -->
<div class="modal modal-form fade" id="contactModal" tabindex="-1" aria-labelledby="contactModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-xl">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close close-custom" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="container-fluid">
                    <?php echo get_template_part('templates/template-modal-form'); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer('landing'); ?>
