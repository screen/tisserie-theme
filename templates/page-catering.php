<?php

/**
 * Template Name: Pagina Catering
 *
 * @package tisserie
 * @subpackage tisserie-mk01-theme
 * @since Mk. 1.0
 */
?>
<?php get_header(); ?>
<?php the_post(); ?>
<main class="container" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row">
        <section class="store-title-content col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <?php the_content(); ?>
        </section>
        <section class="store-tab-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">

            <?php echo get_template_part('templates/template-collapse-title'); ?>

            <div class="inside-tab-title-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <img src="<?php echo get_template_directory_uri(); ?>/images/ornament2.png" alt="Ornament title" class="img-fluid" />
                <h2><?php the_title(); ?></h2>
                <img src="<?php echo get_template_directory_uri(); ?>/images/title-sprite.png" alt="Ornament title" class="img-fluid" />
                <a href="<?php echo get_post_meta(get_the_ID(), 'tsr_store_pdf_download', true); ?>" title="<?php _e('Download PDF', 'tisserie'); ?>" class="btn-pdf"><img src="<?php echo get_template_directory_uri(); ?>/images/pdf-icon.png" alt="<?php _e('Download PDF', 'tisserie'); ?>" class="img-fluid" /> <?php _e('Download', 'tisserie'); ?></a>
            </div>

            <?php $all_locations = get_pages( array( 'post_type' => 'page', 'post_status' => array( 'publish', ) ) );?>
            <?php $cat_group = get_page_children( get_the_ID(), $all_locations ); ?>
            <div class="accordion custom-accordion" id="storeMenu">
                <?php $i = 1; ?>
                <?php foreach ($cat_group as $item) { ?>

                <div class="card">
                    <div class="card-header" id="storeMenu<?php echo $i; ?>">
                        <h2 class="mb-0">
                            <button class="btn btn-link btn-block text-left btn-collapse" type="button" data-target="#collapse<?php echo $i; ?>">
                                <?php echo $item->post_title; ?>
                            </button>
                        </h2>
                    </div>

                    <div id="collapse<?php echo $i; ?>" class="collapse <?php if ($i == 1) { echo ''; } ?>" aria-labelledby="storeMenu<?php echo $i; ?>" data-parent="#storeMenu">
                        <div class="card-body">
                            <div class="catering-main-options-container">
                                <?php $i = 1; $main_options = get_post_meta($item->ID, 'tsr_catering_main_options_group', true); ?>
                                <?php foreach ($main_options as $option) { ?>
                                <div class="catering-main-options-item">
                                    <div class="number">
                                        <div class="number-wrapper"><?php echo $i; ?></div>
                                    </div>
                                    <div class="description">
                                        <?php echo apply_filters('the_content', $option['desc']); ?>
                                    </div>
                                    <div class="price">
                                        $<?php echo $option['price']; ?>
                                    </div>
                                    <div class="additional">
                                        <?php echo $option['additional']; ?>
                                    </div>
                                </div>
                                <?php $i++; } ?>
                            </div>
                            <div class="catering-separator-container">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/separator-catering.png" alt="Separator" class="img-fluid" />
                            </div>

                            <div class="catering-main-options-container">
                                <?php $i = 1; $main_options = get_post_meta($item->ID, 'tsr_catering_general_options_group', true); ?>
                                <?php foreach ($main_options as $option) { ?>
                                <?php if (isset($option['repeat'])) { ?>
                                <?php $class = ($option['repeat'] == 'on') ? 'repeat-options-item': '';?>
                                <?php } else { ?>
                                <?php $class = ''; ?>
                                <?php } ?>
                                <div class="catering-general-options-item <?php echo $class; ?>">
                                    <div class="wrapper">
                                        <div class="description">
                                            <?php echo apply_filters('the_content', $option['desc']); ?>
                                        </div>
                                        <div class="price">
                                            $<?php echo $option['price']; ?>
                                        </div>
                                        <?php if ($option['additional'] != '') { ?>
                                        <div class="additional">
                                            <?php echo $option['additional']; ?>
                                        </div>
                                        <?php } ?>
                                    </div>
                                </div>
                                <?php $i++; } ?>
                            </div>

                            <div class="catering-general-info-container">
                                <div class="frame-info-container">
                                    <?php echo apply_filters( 'the_content', get_post_meta($item->ID, 'tsr_catering_boxed_content', true) ); ?>
                                </div>
                                <div class="last-info-container">
                                    <?php echo apply_filters( 'the_content', get_post_meta($item->ID, 'tsr_catering_main_content', true) ); ?>
                                </div>
                            </div>

                            <div class="catering-contact-data-container">
                                <div class="data-item">
                                    <?php $phone = get_post_meta($item->ID, 'tsr_catering_phone_text', true); ?>
                                    <a href="tel:<?php echo trim($phone, " "); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/phone-icon.png" alt="Phone" class="img-fluid"> <?php echo $phone; ?></a>
                                </div>
                                <div class="data-item">
                                    <?php $email = get_post_meta($item->ID, 'tsr_catering_email_text', true); ?>
                                    <a href="mailto:<?php echo $email; ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/email-icon.png" alt="Phone" class="img-fluid"> <?php echo $email; ?></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php $i++;
                } ?>
            </div>
        </section>
    </div>
</main>
<?php get_footer(); ?>