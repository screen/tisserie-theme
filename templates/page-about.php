<?php

/**
 * Template Name: Pagina About
 *
 * @package tisserie
 * @subpackage tisserie-mk01-theme
 * @since Mk. 1.0
 */
?>
<?php get_header(); ?>
<?php the_post(); ?>
<main class="container-fluid p-0" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row no-gutters">
        <?php $bg_banner_id = get_post_meta(get_the_ID(), 'tsr_about_hero_bg_id', true); ?>
        <?php $bg_banner = wp_get_attachment_image_src($bg_banner_id, 'full', false); ?>
        <section class="about-main-hero-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" style="background: url(<?php echo $bg_banner[0]; ?>);">
            <div class="container">
                <div class="row">
                    <div class="about-main-hero-content col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/ornament.png" alt="Ornament title" class="img-fluid" />
                        <h1><?php echo get_post_meta(get_the_ID(), 'tsr_about_hero_title', true); ?></h1>
                        <img src="<?php echo get_template_directory_uri(); ?>/images/title-sprite.png" alt="Ornament title" class="img-fluid" />
                        <div class="about-main-hero-content-text" data-aos="fade" data-aos-delay="300">
                            <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'tsr_about_hero_desc', true)); ?>
                        </div>

                        <?php $bg_banner_id = get_post_meta(get_the_ID(), 'tsr_about_hero_image_id', true); ?>
                        <?php $bg_banner = wp_get_attachment_image_src($bg_banner_id, 'full', false); ?>
                        <img itemprop="image" content="<?php echo $bg_banner[0]; ?>" src="<?php echo $bg_banner[0]; ?>" title="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true); ?>" alt="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true); ?>" class="img-fluid img-about-hero" width="<?php echo $bg_banner[1]; ?>" height="<?php echo $bg_banner[2]; ?>" data-aos="fade" data-aos-delay="250" />
                    </div>
                </div>
            </div>
        </section>

        <section class="about-history-section col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container-fluid p-0">
                <div class="row no-gutters align-items-center">
                    <div class="about-history-container col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                        <div class="container">
                            <div class="row align-items-center justify-content-center">
                                <div class="about-history-text col-xl-8 offset-xl-3 col-lg-10 offset-lg-1 col-md-12 col-sm-12 col-12" data-aos="fade" data-aos-delay="250">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/ornament.png" alt="Ornament title" class="img-fluid" />
                                    <h2><?php echo get_post_meta(get_the_ID(), 'tsr_about_history_title', true); ?></h2>
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/title-sprite.png" alt="Ornament title" class="img-fluid" />
                                    <div class="about-history-text-desc">
                                        <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'tsr_about_history_desc', true)); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $bg_banner_id = get_post_meta(get_the_ID(), 'tsr_about_history_bg_id', true); ?>
                    <?php $bg_banner = wp_get_attachment_image_src($bg_banner_id, 'full', false); ?>
                    <div class="about-history-container image-content col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12" style="background: url(<?php echo $bg_banner[0]; ?>);" data-aos="fade" data-aos-delay="250">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/about-vector1.png" alt="About" class="img-fluid img-float-top d-xl-block d-lg-block d-md-none d-sm-none d-none" data-aos="fade" data-aos-delay="250"  />

                        <?php $bg_banner_id = get_post_meta(get_the_ID(), 'tsr_about_history_image_id', true); ?>
                        <?php $bg_banner = wp_get_attachment_image_src($bg_banner_id, 'full', false); ?>
                        <img itemprop="image" content="<?php echo $bg_banner[0]; ?>" src="<?php echo $bg_banner[0]; ?>" title="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true); ?>" alt="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true); ?>" class="img-fluid img-about-hero" width="<?php echo $bg_banner[1]; ?>" height="<?php echo $bg_banner[2]; ?>" data-aos="fade" data-aos-delay="250" />

                        <img src="<?php echo get_template_directory_uri(); ?>/images/about-vector2.png" alt="About" class="img-fluid img-float-bottom" />
                    </div>
                </div>
            </div>
        </section>

        <section class="about-descanso-section col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container-fluid p-0">
                <div class="row no-gutters align-items-center">
                    <div class="about-descanso-container col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        <?php $bg_banner_id = get_post_meta(get_the_ID(), 'tsr_about_today_image_id', true); ?>
                        <?php $bg_banner = wp_get_attachment_image_src($bg_banner_id, 'full', false); ?>
                        <img itemprop="image" content="<?php echo $bg_banner[0]; ?>" src="<?php echo $bg_banner[0]; ?>" title="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true); ?>" alt="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true); ?>" class="img-fluid img-about-hero" width="<?php echo $bg_banner[1]; ?>" height="<?php echo $bg_banner[2]; ?>" data-aos="fade" data-aos-delay="250" />
                    </div>
                    <div class="about-descanso-container col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        <div class="container">
                            <div class="row">
                                <div class="about-descanso-text col-xl-7 col-lg-9 col-md-12 col-sm-12 col-12" data-aos="fade" data-aos-delay="350">
                                    <div class="about-descanso-content">
                                        <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'tsr_about_today_desc1', true)); ?>
                                    </div>
                                    <h2><?php echo get_post_meta(get_the_ID(), 'tsr_about_today_title', true); ?></h2>
                                    <div class="about-descanso-content">
                                        <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'tsr_about_today_desc2', true)); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="about-testimonials-section col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row">
                    <div class="about-testimonials-content  col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <?php $slider_group = get_post_meta(get_the_ID(), 'tsr_about_banner_group', true); ?>
                        <div class="about-testimonials-swiper swiper-container">
                            <div class="swiper-wrapper">
                                <?php foreach ($slider_group as $item) { ?>
                                <div class="swiper-slide">
                                    <div class="about-testimonial-item">
                                        <div class="content"><?php echo apply_filters('the_content', $item['desc']); ?></div>
                                        <div class="author"><?php echo $item['author']; ?></div>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <?php $arr_gallery = get_post_meta(get_the_ID(), 'tsr_about_footer_gallery_list', true); ?>
        <?php $i = 1; ?>
        <?php if (!empty($arr_gallery)) : ?>
        <section class="footer-gallery-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container-fluid p-0">
                <div class="row no-gutters minimal">
                    <?php $i = 1; ?>
                    <?php foreach ($arr_gallery as $key => $value) { ?>
                    <article class="footer-gallery-columns col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12">
                        <?php $delay = 50 * $i; ?>
                        <div class="footer-gallery-item" data-aos="fade" data-aos-delay="<?php echo $delay; ?>">
                            <?php $bg_banner = wp_get_attachment_image_src($key, 'boxed_img', false); ?>
                            <img itemprop="image" content="<?php echo $bg_banner[0]; ?>" src="<?php echo $bg_banner[0]; ?>" title="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true); ?>" alt="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true); ?>" class="img-fluid" width="<?php echo $bg_banner[1]; ?>" height="<?php echo $bg_banner[2]; ?>" />
                        </div>
                    </article>
                    <?php $i++;
                        } ?>
                </div>
            </div>
        </section>
        <?php endif; ?>
    </div>
</main>
<?php get_footer(); ?>