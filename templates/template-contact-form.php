<form id="formContact" class="main-contact-form row">
    <div class="custom-contact-input col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
        <input type="text" name="name" id="contact-name" placeholder="<?php _e('NAME', 'tisserie'); ?>" class="form-control custom-form-control" />
        <small class="d-none custom-alert alert-name"></small>
    </div>
    <div class="custom-contact-input col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
        <input type="email" name="email" id="contact-email" placeholder="<?php _e('EMAIL', 'tisserie'); ?>" class="form-control custom-form-control" />
        <small class="d-none custom-alert alert-email"></small>
    </div>
    <div class="custom-contact-input col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
        <input type="phone" name="phone" id="contact-phone" placeholder="<?php _e('PHONE', 'tisserie'); ?>" class="form-control custom-form-control" />
        <small class="d-none custom-alert alert-phone"></small>
    </div>
    <div class="custom-contact-input col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
        <input type="text" name="company-name" id="contact-company-name" placeholder="<?php _e('COMPANY', 'tisserie'); ?>" class="form-control custom-form-control" />
        <small class="d-none custom-alert alert-company-name"></small>
    </div>
    <div class="custom-contact-input col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <input type="text" name="subject" id="contact-subject" placeholder="<?php _e('SUBJECT', 'tisserie'); ?>" class="form-control custom-form-control" />
        <small class="d-none custom-alert alert-subject"></small>
    </div>
    <div class="custom-contact-input col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <textarea name="message" id="contact-message" cols="30" rows="5"  placeholder="<?php _e('MESSAGE', 'tisserie'); ?>" class="form-control custom-form-control" ></textarea>
        <small class="d-none custom-alert alert-message"></small>
    </div>

    <div class="custom-contact-submit col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <button type="submit" class="btn btn-lg btn-yellow"><?php _e('Send', 'tisserie'); ?></button>
    </div>
    <div class="custom-contact-response col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="loader-css d-none"></div>
    </div>
</form>
