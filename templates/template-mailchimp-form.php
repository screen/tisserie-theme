<form class="mailchimp-form-container container p-0">
    <div class="row align-items-center">
        <div class="mailchimp-form-item col-6">
            <input type="email" title="<?php _e('Enter your Email Address', 'tisserie'); ?>" name="EMAIL" required placeholder="<?php _e('E-MAIL', 'tisserie'); ?>" class="form-control" />
        </div>
        <div class="mailchimp-form-item col-6">
            <button type="submit" title="<?php echo get_post_meta(get_the_ID(), 'tsr_home_subscription_button_text', true); ?>" class="btn btn-md btn-subscribe"><?php echo get_post_meta(get_the_ID(), 'tsr_home_subscription_button_text', true); ?></button>
        </div>
    </div>
</form>
