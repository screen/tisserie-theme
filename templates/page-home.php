<?php

/**
 * Template Name: Main Home
 *
 * @package tisserie
 * @subpackage tisserie-mk01-theme
 * @since Mk. 1.0
 */
?>
<?php get_header(); ?>
<?php the_post(); ?>
<main class="container-fluid p-0" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row no-gutters">
        <section class="main-slider-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="main-home-slider-container swiper-container">
                <div class="swiper-wrapper">
                    <?php $arr_testimonials = get_post_meta(get_the_ID(), 'tsr_home_slider_group', true); ?>
                    <?php if (!empty($arr_testimonials)) : ?>
                    <?php foreach ($arr_testimonials as $item) { ?>
                    <div class="swiper-slide">
                        <div class="container-fluid p-0">
                            <div class="row align-items-start no-gutters">
                                <div class="main-slider-content col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" data-aos="fade" data-aos-delay="350">
                                    <?php $bg_banner = wp_get_attachment_image_src($item['bg_image_id'], 'full', false); ?>
                                    <img itemprop="image" content="<?php echo $bg_banner[0]; ?>" src="<?php echo $bg_banner[0]; ?>" title="<?php echo get_post_meta($item['bg_image_id'], '_wp_attachment_image_alt', true); ?>" alt="<?php echo get_post_meta($item['bg_image_id'], '_wp_attachment_image_alt', true); ?>" class="img-fluid" width="<?php echo $bg_banner[1]; ?>" height="<?php echo $bg_banner[2]; ?>" />
                                    <div class="main-slider-info-wrapper">
                                        <h2><?php echo $item['title']; ?></h2>
                                        <?php echo apply_filters('the_content', $item['desc']); ?>
                                        <?php $url = $item['button_link']; ?>
                                        <?php if ($item['button_link'] != '') { ?>
                                        <a href="<?php echo $item['button_link']; ?>" class="btn btn-md btn-yellow"><?php echo $item['button_text']; ?></a>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                    <?php endif; ?>
                </div>
                <div class="swiper-pagination"></div>
            </div>
        </section>
        <?php $bg_banner_id = get_post_meta(get_the_ID(), 'tsr_home_second_section_bg_id', true); ?>
        <?php $bg_banner = wp_get_attachment_image_src($bg_banner_id, 'full', false); ?>
        <section class="home-new-about-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" style="background: url(<?php echo $bg_banner[0]; ?>);">
            <div class="container">
                <div class="row">
                    <div class="home-new-about-title-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 order-xl-1 order-lg-1 order-md-1 order-sm-1 order-1">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/ornament.png" alt="Ornament title" class="img-fluid" />
                        <h2><?php echo get_post_meta(get_the_ID(), 'tsr_home_second_section_title', true); ?></h2>
                        <img src="<?php echo get_template_directory_uri(); ?>/images/title-sprite.png" alt="Ornament title" class="img-fluid" />
                    </div>
                    <div class="home-new-about-content col-xl-4 col-lg-5 col-md-12 col-sm-12 col-12 order-xl-2 order-lg-2 order-md-3 order-sm-3 order-3">
                        <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'tsr_home_second_section_desc', true)); ?>
                        <h3><?php _e('AS SEEN ON', 'tisserie'); ?></h3>
                        <div class="home-new-about-logos-container">
                            <?php $arr_gallery = get_post_meta(get_the_ID(), 'tsr_home_second_section_logos', true); ?>
                            <?php $i = 1; ?>
                            <?php if (!empty($arr_gallery)) : ?>
                            <?php foreach ($arr_gallery as $key => $value) { ?>
                            <div class="home-new-about-item home-new-about-item-<?php echo $i; ?>">
                                <?php $delay = 80 * $i; ?>
                                <?php $bg_banner = wp_get_attachment_image_src($key, 'full', false); ?>
                                <img itemprop="image" content="<?php echo $bg_banner[0]; ?>" src="<?php echo $bg_banner[0]; ?>" title="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true); ?>" alt="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true); ?>" class="img-fluid" width="<?php echo $bg_banner[1]; ?>" height="<?php echo $bg_banner[2]; ?>" data-aos="fade" data-aos-delay="<?php echo $delay; ?>" />
                            </div>
                            <?php $i++;
                                } ?>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="home-new-about-image col-xl-8 col-lg-7 col-md-12 col-sm-12 col-12 order-xl-3 order-lg-3 order-md-2 order-sm-2 order-2">
                        <?php $bg_banner_id = get_post_meta(get_the_ID(), 'tsr_home_second_section_image_id', true); ?>
                        <?php $bg_banner = wp_get_attachment_image_src($bg_banner_id, 'full', false); ?>
                        <img itemprop="image" content="<?php echo $bg_banner[0]; ?>" src="<?php echo $bg_banner[0]; ?>" title="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true); ?>" alt="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true); ?>" class="img-fluid" width="<?php echo $bg_banner[1]; ?>" height="<?php echo $bg_banner[2]; ?>" data-aos="fade" data-aos-delay="150" />
                    </div>
                </div>
            </div>
        </section>
        <?php $bg_banner_id = get_post_meta(get_the_ID(), 'tsr_home_menu_section_bg_id', true); ?>
        <?php $bg_banner = wp_get_attachment_image_src($bg_banner_id, 'full', false); ?>
        <section class="home-menu-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container-fluid p-0">
                <div class="row no-gutters">
                    <div class="home-menu-image col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 d-xl-block d-lg-block d-md-block d-sm-none d-none" style="background: url(<?php echo $bg_banner[0]; ?>);">
                        <div class="home-menu-logos-container">
                            <?php $arr_gallery = get_post_meta(get_the_ID(), 'tsr_home_menu_section_images', true); ?>
                            <?php $i = 1; ?>
                            <?php if (!empty($arr_gallery)) : ?>
                            <?php foreach ($arr_gallery as $key => $value) { ?>
                            <?php $delay = 80 * $i; ?>
                            <div class="home-menu-item home-menu-item-<?php echo $i; ?>" data-aos="fade-right" data-aos-delay="<?php echo $delay; ?>">
                                <?php $bg_banner = wp_get_attachment_image_src($key, 'full', false); ?>
                                <img itemprop="image" content="<?php echo $bg_banner[0]; ?>" src="<?php echo $bg_banner[0]; ?>" title="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true); ?>" alt="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true); ?>" class="img-fluid" width="<?php echo $bg_banner[1]; ?>" height="<?php echo $bg_banner[2]; ?>" />
                            </div>
                            <?php $i++;
                                } ?>
                            <?php endif; ?>
                        </div>
                        <div class="container">
                            <div class="row">
                                <div class="home-menu-images-wrapper col-xl-6 offset-xl-6 col-lg-6 offset-lg-6 col-md-12 col-sm-12 col-12">

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="home-menu-content col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        <div class="container">
                            <div class="row">
                                <div class="home-menu-content-wrapper col-xl-8 col-lg-10 col-md-12 col-sm-12 col-12" data-aos="fade" data-aos-delay="450">
                                    <div class="home-menu-title-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <img src="<?php echo get_template_directory_uri(); ?>/images/ornament2.png" alt="Ornament title" class="img-fluid" />
                                        <h2><?php echo get_post_meta(get_the_ID(), 'tsr_home_menu_section_title', true); ?></h2>
                                        <img src="<?php echo get_template_directory_uri(); ?>/images/title-sprite.png" alt="Ornament title" class="img-fluid" />
                                    </div>
                                    <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'tsr_home_menu_section_desc', true)); ?>
                                    <a href="<?php echo get_post_meta(get_the_ID(), 'tsr_home_menu_section_btn_link', true); ?>" class="btn btn-md btn-yellow"><?php echo get_post_meta(get_the_ID(), 'tsr_home_menu_section_btn_text', true); ?></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="benefits-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row">
                    <div class="home-benefits-title-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" data-aos="fade" data-aos-delay="250">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/ornament2.png" alt="Ornament title" class="img-fluid" />
                        <h2><?php echo get_post_meta(get_the_ID(), 'tsr_home_benefits_section_title', true); ?></h2>
                        <img src="<?php echo get_template_directory_uri(); ?>/images/title-sprite.png" alt="Ornament title" class="img-fluid" />
                    </div>
                    <div class="home-benefits-desc-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" data-aos="fade" data-aos-delay="300">
                        <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'tsr_home_benefits_section_desc', true)); ?>
                    </div>
                    <div class="home-benefits-items-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="container">
                            <div class="row">
                                <?php $arr_benefits = get_post_meta(get_the_ID(), 'tsr_home_benefits_group', true); ?>
                                <?php $i = 1; ?>
                                <?php if (!empty($arr_benefits)) : ?>
                                <?php foreach ($arr_benefits as $item) { ?>
                                <?php $i = ($i > 3) ?  1 : $i;  ?>
                                <?php $delay = 250 * $i; ?>
                                <div class="home-benefits-item col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12" data-aos="fade" data-aos-delay="<?php echo $delay; ?>">
                                    <?php $bg_banner = wp_get_attachment_image_src($item['bg_image_id'], 'full', false); ?>
                                    <img itemprop="image" content="<?php echo $bg_banner[0]; ?>" src="<?php echo $bg_banner[0]; ?>" title="<?php echo get_post_meta($item['bg_image_id'], '_wp_attachment_image_alt', true); ?>" alt="<?php echo get_post_meta($item['bg_image_id'], '_wp_attachment_image_alt', true); ?>" class="img-fluid" width="<?php echo $bg_banner[1]; ?>" height="<?php echo $bg_banner[2]; ?>" />
                                    <?php echo apply_filters('the_content', $item['title']); ?>
                                </div>
                                <?php $i++;
                                    } ?>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="testimonials-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row">
                    <div class="testimonials-main-title-content col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" data-aos="fade" data-aos-delay="250">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/ornament.png" alt="Ornament title" class="img-fluid" />
                        <h2><?php echo get_post_meta(get_the_ID(), 'tsr_home_test_section_title', true); ?></h2>
                        <img src="<?php echo get_template_directory_uri(); ?>/images/title-sprite.png" alt="Ornament title" class="img-fluid" />
                    </div>
                    <div class="testimonials-main-desc-content col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" data-aos="fade" data-aos-delay="350">
                        <h3><?php _e('Top 20 in Coffee & Tea on New York City.', 'tisserie'); ?></h3>
                        <img src="<?php echo get_template_directory_uri(); ?>/images/tripadvisor-logo.png" alt="TripAdvisors" class="img-fluid" />
                        <img src="<?php echo get_template_directory_uri(); ?>/images/tripadvisor-choice.png" alt="TripAdvisors Choice" class="img-fluid img-logo-float" />
                        <div class="star-container">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                        </div>
                    </div>
                    <div class="testimonials-group-content col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="container">
                            <div class="row">
                                <div class="testimonial-group-items col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <div class="testimonial-swiper-container swiper-container">
                                        <div class="swiper-wrapper">
                                            <?php $arr_testimonials = get_post_meta(get_the_ID(), 'tsr_home_test_group', true); ?>
                                            <?php foreach ($arr_testimonials as $item) { ?>
                                            <div class="swiper-slide">
                                                <div class="testimonial-item">
                                                    <div class="testimonial-item-wrapper">
                                                        <?php $bg_banner = wp_get_attachment_image_src($item['image_id'], 'full', false); ?>
                                                        <a href="https://www.tripadvisor.com/Restaurant_Review-g60763-d827442-Reviews-Tisserie-New_York_City_New_York.html" target="_blank">
                                                            <img itemprop="image" content="<?php echo $bg_banner[0]; ?>" src="<?php echo $bg_banner[0]; ?>" title="<?php echo get_post_meta($item['image_id'], '_wp_attachment_image_alt', true); ?>" alt="<?php echo get_post_meta($item['image_id'], '_wp_attachment_image_alt', true); ?>" class="img-fluid" width="<?php echo $bg_banner[1]; ?>" height="<?php echo $bg_banner[2]; ?>" />
                                                        </a>
                                                        <div class="text-wrapper">
                                                            <h3><?php echo $item['author']; ?></h3>
                                                            <?php echo apply_filters('the_content', $item['desc']); ?>
                                                            <div class="star-container">
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php } ?>
                                        </div>
                                        <div class="swiper-button-prev"></div>
                                        <div class="swiper-button-next"></div>
                                    </div>
                                </div>
                                <div class="testimonials-button-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" data-aos="fade" data-aos-delay="250">
                                    <a href="https://www.tripadvisor.com/Restaurant_Review-g60763-d827442-Reviews-Tisserie-New_York_City_New_York.html" target="_blank" class="btn btn-md btn-yellow"><?php _e('See More reviews', 'tisserie'); ?></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <?php $bg_banner_id = get_post_meta(get_the_ID(), 'tsr_home_descanso_section_bg_id', true); ?>
        <?php $bg_banner = wp_get_attachment_image_src($bg_banner_id, 'full', false); ?>
        <section class="home-footer-hero-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" style="background: url(<?php echo $bg_banner[0]; ?>);">
            <div class="container">
                <div class="row">
                    <div class="home-footer-hero-content col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" data-aos="fade" data-aos-delay="350">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/ornament3.png" alt="Ornament title" class="img-fluid" />
                        <h2><?php echo get_post_meta(get_the_ID(), 'tsr_home_descanso_section_title', true); ?></h2>
                        <a href="<?php echo get_post_meta(get_the_ID(), 'tsr_home_descanso_section_btn_link', true); ?>" class="btn btn-md btn-yellow"><?php echo get_post_meta(get_the_ID(), 'tsr_home_descanso_section_btn_text', true); ?></a>
                    </div>
                </div>
            </div>
        </section>

        <?php $arr_gallery = get_post_meta(get_the_ID(), 'tsr_home_footer_gallery_list', true); ?>
        <?php $i = 1; ?>
        <?php if (!empty($arr_gallery)) : ?>
        <section class="footer-gallery-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container-fluid p-0">
                <div class="row no-gutters minimal">
                    <?php $i = 1; ?>
                    <?php foreach ($arr_gallery as $key => $value) { ?>
                    <article class="footer-gallery-columns col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12">
                        <?php $delay = 50 * $i; ?>
                        <div class="footer-gallery-item" data-aos="fade" data-aos-delay="<?php echo $delay; ?>">
                            <?php $bg_banner = wp_get_attachment_image_src($key, 'boxed_img', false); ?>
                            <img itemprop="image" content="<?php echo $bg_banner[0]; ?>" src="<?php echo $bg_banner[0]; ?>" title="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true); ?>" alt="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true); ?>" class="img-fluid" width="<?php echo $bg_banner[1]; ?>" height="<?php echo $bg_banner[2]; ?>" />
                        </div>
                    </article>
                    <?php $i++;
                        } ?>
                </div>
            </div>
        </section>
        <?php endif; ?>
    </div>
</main>
<!-- Modal -->
<div class="modal modal-form fade" id="contactModal" tabindex="-1" aria-labelledby="contactModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-xl">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close close-custom" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="container-fluid">
                    <?php echo get_template_part('templates/template-modal-form'); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>