<?php

/**
 * Template Name: Pagina Delivery
 *
 * @package tisserie
 * @subpackage tisserie-mk01-theme
 * @since Mk. 1.0
 */
?>
<?php get_header(); ?>
<?php the_post(); ?>
<main class="container-fluid p-0" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row no-gutters">
        <?php $bg_banner_id = get_post_meta(get_the_ID(), 'tsr_delivery_hero_bg_id', true); ?>
        <?php $bg_banner = wp_get_attachment_image_src($bg_banner_id, 'full', false); ?>
        <section class="delivery-main-hero-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" style="background: url(<?php echo $bg_banner[0]; ?>);">
            <div class="container">
                <div class="row">
                    <div class="delivery-main-hero-content col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/ornament.png" alt="Ornament title" class="img-fluid" />
                        <h1><?php echo get_post_meta(get_the_ID(), 'tsr_delivery_hero_title', true); ?></h1>
                        <img src="<?php echo get_template_directory_uri(); ?>/images/title-sprite.png" alt="Ornament title" class="img-fluid" />
                        <div class="delivery-main-hero-content-text">
                            <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'tsr_delivery_hero_desc', true)); ?>
                        </div>
                        <?php $bg_banner_id = get_post_meta(get_the_ID(), 'tsr_about_hero_image_id', true); ?>
                        <?php $bg_banner = wp_get_attachment_image_src($bg_banner_id, 'full', false); ?>
                        <?php $arr_gallery = get_post_meta(get_the_ID(), 'tsr_delivery_hero_logo_list', true); ?>
                        <?php $i = 1; ?>
                        <?php if (!empty($arr_gallery)) : ?>
                        <section class="delivery-logo-gallery-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="container-fluid">
                                <div class="row justify-content-center">
                                    <?php $i = 1; ?>
                                    <?php foreach ($arr_gallery as $item) { ?>
                                    <article class="delivery-gallery-columns col-xl-3 col-lg-3 col-md-4 col-sm-6 col-12">
                                        <?php $delay = 50 * $i; ?>
                                        <div class="delivery-gallery-item" data-aos="fade-up" data-aos-delay="<?php echo $delay; ?>">
                                            <?php $bg_banner_id = $item['logo_id']; ?>
                                            <?php $bg_banner = wp_get_attachment_image_src($bg_banner_id, 'full', false); ?>
                                            <a href="<?php echo $item['link'] ?>" target="_blank">
                                                <img itemprop="image" content="<?php echo $bg_banner[0]; ?>" src="<?php echo $bg_banner[0]; ?>" title="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true); ?>" alt="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true); ?>" class="img-fluid" width="<?php echo $bg_banner[1]; ?>" height="<?php echo $bg_banner[2]; ?>" />
                                            </a>
                                        </div>
                                    </article>
                                    <?php $i++;
                        } ?>
                                </div>
                            </div>
                        </section>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </section>
    </div>
</main>
<?php get_footer(); ?>