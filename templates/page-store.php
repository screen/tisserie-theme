<?php

/**
 * Template Name: Pagina Store Menu
 *
 * @package tisserie
 * @subpackage tisserie-mk01-theme
 * @since Mk. 1.0
 */
?>
<?php get_header(); ?>
<?php the_post(); ?>
<main class="container" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row">
        <section class="store-title-content col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <?php the_content(); ?>
        </section>
        <section class="store-tab-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">

            <?php echo get_template_part('templates/template-collapse-title'); ?>

            <div class="inside-tab-title-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <img src="<?php echo get_template_directory_uri(); ?>/images/ornament2.png" alt="Ornament title" class="img-fluid" />
                <h2><?php the_title(); ?></h2>
                <img src="<?php echo get_template_directory_uri(); ?>/images/title-sprite.png" alt="Ornament title" class="img-fluid" />
                <a href="<?php echo get_post_meta(get_the_ID(), 'tsr_store_pdf_download', true); ?>" title="<?php _e('Download PDF', 'tisserie'); ?>" class="btn-pdf"><img src="<?php echo get_template_directory_uri(); ?>/images/pdf-icon.png" alt="<?php _e('Download PDF', 'tisserie'); ?>" class="img-fluid" /> <?php _e('Download', 'tisserie'); ?></a>
            </div>


            <?php $cat_group = get_post_meta(get_the_ID(), 'tsr_store_taxonomy_select', true); ?>
            <div class="accordion custom-accordion" id="storeMenu">
                <?php $i = 1; ?>
                <?php foreach ($cat_group as $item) { ?>
                <?php $current_cat = get_term_by('id', $item, 'product_cat'); ?>
                <div class="card">
                    <div class="card-header" id="storeMenu<?php echo $i; ?>">
                        <h2 class="mb-0">
                            <button class="btn btn-link btn-block btn-collapse text-left" type="button" data-target="#collapse<?php echo $i; ?>">
                                <?php echo $current_cat->name; ?>
                            </button>
                        </h2>
                    </div>

                    <div id="collapse<?php echo $i; ?>" class="collapse <?php if ($i == 1) { echo ''; } ?>" aria-labelledby="storeMenu<?php echo $i; ?>" data-parent="#storeMenu">
                        <div class="card-body">
                            <?php $arr_products = new WP_Query(array('post_type' => 'product', 'posts_per_page' => -1, 'order' => 'ASC', 'orderby' => 'date', 'tax_query' => array(array('taxonomy' => 'product_cat', 'field' => 'term_id', 'terms' => array($current_cat->term_id))))); ?>
                            <?php if ($arr_products->have_posts()) : ?>
                            <?php while ($arr_products->have_posts()) : $arr_products->the_post(); ?>
                            <div class="product-collapse-item">
                                <div class="product-collapse-wrapper">
                                    <h3><?php the_title(); ?></h3>
                                    <?php echo get_the_excerpt(); ?>
                                </div>
                                <div class="price-collapse-wrapper">
                                    <?php $price = get_post_meta(get_the_ID(), '_regular_price', true); ?>
                                    <?php if ($price != '') { ?>
                                    <?php echo get_woocommerce_currency_symbol(); ?>
                                    <?php echo number_format(get_post_meta(get_the_ID(), '_regular_price', true), 2, '.', ' '); ?>
                                    <?php } ?>
                                </div>
                            </div>
                            <?php endwhile; ?>
                            <?php endif; ?>
                            <?php wp_reset_query(); ?>
                        </div>
                    </div>
                </div>
                <?php $i++;
                } ?>
            </div>
        </section>
    </div>
</main>
<?php get_footer(); ?>