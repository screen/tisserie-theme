<?php
/**
* Template Name: Thanks Landing
*
* @package tisserie
* @subpackage tisserie-mk01-theme
* @since Mk. 1.0
*/
?>
<?php get_header(); ?>
<?php the_post(); ?>
<main class="container-fluid p-0" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row no-gutters">
        <?php $bg_banner_id = get_post_meta(get_the_ID(), 'tsr_thanks_banner_bg_id', true); ?>
        <?php $bg_banner = wp_get_attachment_image_src($bg_banner_id, 'full', false); ?>
        <?php var_dump($bg_banner); ?>
        <section class="thanks-hero-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" style="background: url(<?php echo $bg_banner[0]; ?>);">
            <div class="container">
                <div class="row align-items-center">
                    <div class="thanks-hero-content col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <?php $bg_banner_id = get_post_meta(get_the_ID(), 'tsr_thanks_banner_logo_id', true); ?>
                        <?php if ($bg_banner_id != '') { ?>
                        <?php $bg_banner = wp_get_attachment_image_src($bg_banner_id, 'full', false); ?>
                        <img itemprop="image" content="<?php echo $bg_banner[0];?>" src="<?php echo $bg_banner[0];?>" title="<?php echo get_post_meta( $bg_banner_id, '_wp_attachment_image_alt', true ); ?>" alt="<?php echo get_post_meta($bg_banner_id, '_wp_attachment_image_alt', true ); ?>" class="img-fluid" width="<?php echo $bg_banner[1]; ?>" height="<?php echo $bg_banner[2]; ?>" />
                        <?php } else { ?>
                        <svg class="thanks-svg" version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 130.2 130.2">
                            <circle class="path circle" fill="none" stroke="#C6A144" stroke-width="7" stroke-miterlimit="10" cx="65.1" cy="65.1" r="62.1" />
                            <polyline class="path check" fill="none" stroke="#C6A144" stroke-width="7" stroke-linecap="round" stroke-miterlimit="10" points="100.2,40.2 51.5,88.8 29.8,67.5 " />
                        </svg>
                        <?php } ?>
                        <?php the_content(); ?>
                        <div class="thanks-social-container">
                            <?php $social_options = get_option('tsr_social_settings'); ?>
                            <?php if ((isset($social_options['facebook'])) && ($social_options['facebook'] != '')) { ?>
                            <a href="<?php echo $social_options['facebook']; ?>" title="<?php _e('Click here to visit our profile', 'tisserie'); ?>" target="_blank"><i class="fa fa-facebook"></i></a>
                            <?php } ?>

                            <?php if ((isset($social_options['twitter'])) && ($social_options['twitter'] != '')) { ?>
                            <a href="<?php echo $social_options['twitter']; ?>" title="<?php _e('Click here to visit our profile', 'tisserie'); ?>" target="_blank"><i class="fa fa-twitter"></i></a>
                            <?php } ?>

                            <?php if ((isset($social_options['instagram'])) && ($social_options['instagram'] != '')) { ?>
                            <a href="<?php echo $social_options['instagram']; ?>" title="<?php _e('Click here to visit our profile', 'tisserie'); ?>" target="_blank"><i class="fa fa-instagram"></i></a>
                            <?php } ?>

                            <?php if ((isset($social_options['pinterest'])) && ($social_options['pinterest'] != '')) { ?>
                            <a href="<?php echo $social_options['pinterest']; ?>" title="<?php _e('Click here to visit our profile', 'tisserie'); ?>" target="_blank"><i class="fa fa-pinterest"></i></a>
                            <?php } ?>

                            <?php if ((isset($social_options['linkedin'])) && ($social_options['linkedin'] != '')) { ?>
                            <a href="<?php echo $social_options['linkedin']; ?>" title="<?php _e('Click here to visit our profile', 'tisserie'); ?>" target="_blank"><i class="fa fa-linkedin"></i></a>
                            <?php } ?>

                            <?php if ((isset($social_options['youtube'])) && ($social_options['youtube'] != '')) { ?>
                            <a href="<?php echo $social_options['youtube']; ?>" title="<?php _e('Click here to visit our profile', 'tisserie'); ?>" target="_blank"><i class="fa fa-youtube-play"></i></a>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</main>
<?php get_footer(); ?>
