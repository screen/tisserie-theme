<?php

/**
 * Template Name: Pagina Contacto
 *
 * @package tisserie
 * @subpackage tisserie-mk01-theme
 * @since Mk. 1.0
 */
?>
<?php get_header(); ?>
<?php the_post(); ?>
<main class="container-fluid p-0" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row no-gutters">
        <?php $bg_banner_id = get_post_meta(get_the_ID(), 'tsr_contact_hero_bg_id', true); ?>
        <?php $bg_banner = wp_get_attachment_image_src($bg_banner_id, 'full', false); ?>
        <section class="contact-main-hero-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" style="background: url(<?php echo $bg_banner[0]; ?>);">
            <div class="container">
                <div class="row">
                    <div class="contact-main-hero-content col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/conrtactornament.png" alt="Ornament title" class="img-fluid" />
                        <h1><?php _e('Contact Us', 'tisserie'); ?></h1>
                        <img src="<?php echo get_template_directory_uri(); ?>/images/contact-line.png" alt="Ornament title" class="img-fluid" />
                    </div>
                </div>
                <?php echo get_template_part('templates/template-contact-form'); ?>
            </div>
        </section>
    </div>
</main>
<?php get_footer(); ?>