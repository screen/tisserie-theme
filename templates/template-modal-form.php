<?php $modal_options = get_option('tsr_modal_settings'); ?>
<form id="formModal" class="main-modal-form row">
    <div class="custom-modal-title col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <h2><?php echo $modal_options['modal_title']; ?></h2>

        <?php echo apply_filters('the_content', $modal_options['modal_text']); ?>
    </div>
    <div class="custom-modal-input col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
        <input type="text" name="name" id="modal-name" placeholder="<?php _e('NAME', 'tisserie'); ?>" class="form-control custom-form-control" />
        <small class="d-none custom-alert alert-name"></small>
    </div>
    <div class="custom-modal-input col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
        <input type="email" name="email" id="modal-email" placeholder="<?php _e('EMAIL', 'tisserie'); ?>" class="form-control custom-form-control" />
        <small class="d-none custom-alert alert-email"></small>
    </div>
    <div class="custom-modal-input col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
        <input type="phone" name="phone" id="modal-phone" placeholder="<?php _e('PHONE', 'tisserie'); ?>" class="form-control custom-form-control" />
        <small class="d-none custom-alert alert-phone"></small>
    </div>
    <div class="custom-modal-input col-xl-12 col-lg-12 col-md-12 col-sm-6 col-12">
        <input type="text" name="address" id="modal-address" placeholder="<?php _e('ADDRESS', 'tisserie'); ?>" class="form-control custom-form-control" />
        <small class="d-none custom-alert alert-address"></small>
    </div>

    <div class="custom-modal-input col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
        <input type="text" name="role" id="modal-role" placeholder="<?php _e('ROLE IN THE COMPANY', 'tisserie'); ?>" class="form-control custom-form-control" />
        <small class="d-none custom-alert alert-role"></small>
    </div>

    <div class="custom-modal-input col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
        <input type="text" name="company-name" id="modal-company-name" placeholder="<?php _e('NAME OF THE COMPANY', 'tisserie'); ?>" class="form-control custom-form-control" />
        <small class="d-none custom-alert alert-company-name"></small>
    </div>
    <div class="custom-modal-input col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
        <input type="text" name="company-size" id="modal-company-size" placeholder="<?php _e('SIZE OF THE COMPANY', 'tisserie'); ?>" class="form-control custom-form-control" />
        <small class="d-none custom-alert alert-company-size"></small>
    </div>

    <div class="custom-modal-input col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
        <select name="business-type" id="modal-business-type" class="form-control custom-form-control">
            <option value="" selected disabled><?php _e('BUSINESS TYPE', 'tisserie'); ?></option>
            <option value="Hotels"><?php _e('Hotels', 'tisserie'); ?></option>
            <option value="Restaurants"><?php _e('Restaurants', 'tisserie'); ?></option>
            <option value="Hospitals"><?php _e('Hospitals', 'tisserie'); ?></option>
            <option value="Business Offices"><?php _e('Business Offices', 'tisserie'); ?></option>
            <option value="Supermarkets"><?php _e('Supermarkets', 'tisserie'); ?></option>
            <option value="Coffee Shops"><?php _e('Coffee Shops', 'tisserie'); ?></option>
        </select>
        <small class="d-none custom-alert alert-business-type"></small>
    </div>
    <div class="custom-modal-input col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
        <select name="frequency" id="modal-frequency" class="form-control custom-form-control">
            <option value="" selected disabled><?php _e('FREQUENCY OF THE DELIVER', 'tisserie'); ?></option>
            <option value="dayly"><?php _e('Dayly', 'tisserie'); ?></option>
            <option value="weekly"><?php _e('Weekly', 'tisserie'); ?></option>
            <option value="monthly"><?php _e('Monthly', 'tisserie'); ?></option>
        </select>
        <small class="d-none custom-alert alert-frequency"></small>
    </div>

    <div class="custom-modal-submit col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <button type="submit" class="btn btn-lg btn-yellow"><?php _e('Send', 'tisserie'); ?></button>
    </div>

    <div class="custom-modal-response col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="loader-css d-none"></div>
    </div>
</form>
