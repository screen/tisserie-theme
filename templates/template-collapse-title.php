<div class="collapse-title-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
    <ul>
        <?php $slug = get_post_field( 'post_name'); ?>
        <li><a href="<?php echo home_url('/store-menu'); ?>" class="<?php if ($slug == 'store-menu') { echo 'active'; } ?>"><?php _e('Store Menu', 'tisserie'); ?></a></li>
        <li><a href="<?php echo home_url('/wholesale'); ?>" class="<?php if ($slug == 'wholesale') { echo 'active'; } ?>"><?php _e('Wholesale', 'tisserie'); ?></a></li>
        <li><a href="<?php echo home_url('/catering'); ?>" class="<?php if ($slug == 'catering') { echo 'active'; } ?>"><?php _e('Catering', 'tisserie'); ?></a></li>
    </ul>
</div>