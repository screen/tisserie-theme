<!DOCTYPE html>
<html <?php language_attributes() ?>>

<head>
    <?php /* MAIN STUFF */ ?>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=<?php bloginfo('charset') ?>" />
    <meta name="robots" content="NOODP, INDEX, FOLLOW" />
    <meta name="HandheldFriendly" content="True" />
    <meta name="MobileOptimized" content="320" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="pingback" href="<?php echo esc_url(get_bloginfo('pingback_url')); ?>" />
    <link rel="profile" href="http://gmpg.org/xfn/11" />
    <link rel="dns-prefetch" href="//facebook.com" crossorigin />
    <link rel="dns-prefetch" href="//connect.facebook.net" crossorigin />
    <link rel="dns-prefetch" href="//ajax.googleapis.com" crossorigin />
    <link rel="dns-prefetch" href="//google-analytics.com" crossorigin />
    <?php /* FAVICONS */ ?>
    <link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/images/apple-touch-icon.png" />
    <?php /* THEME NAVBAR COLOR */ ?>
    <meta name="msapplication-TileColor" content="#E1BE64" />
    <meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/images/win8-tile-icon.png" />
    <meta name="theme-color" content="#E1BE64" />
    <?php /* AUTHOR INFORMATION */ ?>
    <meta name="language" content="<?php echo get_bloginfo('language'); ?>" />
    <meta name="DC.title" content="<?php if (is_home()) { echo get_bloginfo('name') . ' | ' . get_bloginfo('description'); } else { echo get_the_title() . ' | ' . get_bloginfo('name'); } ?>" />
    <?php /* MAIN TITLE - CALL HEADER MAIN FUNCTIONS */ ?>
    <?php wp_title('|', false, 'right'); ?>
    <?php wp_head() ?>
    <?php /* OPEN GRAPHS INFO - COMMENTS SCRIPTS */ ?>
    <?php if ( is_single('post') && get_option( 'thread_comments' ) ) wp_enqueue_script( 'comment-reply' ); ?>
    <?php /* IE COMPATIBILITIES */ ?>
    <!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7" /><![endif]-->
    <!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8" /><![endif]-->
    <!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9" /><![endif]-->
    <!--[if gt IE 8]><!-->
    <html <?php language_attributes(); ?> class="no-js" />
    <!--<![endif]-->
    <!--[if IE]> <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script> <![endif]-->
    <!--[if IE]> <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script> <![endif]-->
    <!--[if IE]> <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico" /> <![endif]-->
</head>

<body class="the-main-body <?php echo join(' ', get_body_class()); ?>" itemscope itemtype="http://schema.org/WebPage">
    <?php wp_body_open(); ?>
    <?php $modal_options = get_option('tsr_modal_settings'); ?>
    <?php $page_id = get_queried_object_id(); ?>
    <?php $blackmode = get_post_meta($page_id, 'tsr_header_black', true); ?>
    <header class="container-fluid p-0" role="banner" itemscope itemtype="http://schema.org/WPHeader">
        <div class="row no-gutters">
            <div class="the-header <?php if ($blackmode == 'on') { echo 'header-dark'; } ?> col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="header-left col-xl-3 col-lg-3 col-md-3 col-sm-5 col-9 order-xl-1 order-lg-1 order-md-2 order-sm-2 order-2">
                            <a href="<?php echo home_url('/'); ?>" title="<?php _e('Volver al inicio', 'tisserie'); ?>">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/logo-box.png" alt="Logo" class="img-fluid img-logo-header" />
                            </a>
                        </div>
                        <div class="header-middle col-xl-6 col-lg-6 col-md-9 col-sm-7 col-12 order-xl-2 order-lg-2 order-md-3 order-sm-3 order-3 d-xl-block d-lg-block d-md-block d-sm-none d-none">
                            <?php wp_nav_menu( array(
                            'theme_location'  => 'header_menu',
                            'depth'           => 2,
                            'container'       => 'div'
                        ) );
                        ?>
                        </div>
                        <div class="header-button-mobile col-xl-6 col-lg-6 col-md-9 col-sm-7 col-3 order-xl-2 order-lg-2 order-md-3 order-sm-3 order-3 d-xl-none d-lg-none d-md-none d-sm-block d-block">
                            <button class="menu-opener"><i class="fa fa-bars"></i></button>
                        </div>
                        <div class="header-menu-mobile menu-mobile-hidden col-xl-6 col-lg-6 col-md-9 col-sm-12 col-12 order-xl-2 order-lg-2 order-md-3 order-sm-3 order-3">
                            <?php wp_nav_menu( array(
                            'theme_location'  => 'header_menu',
                            'depth'           => 2,
                            'container'       => 'div'
                        ) );
                        ?>
                            <a href="<?php echo home_url('/delivery'); ?>" class="btn btn-md btn-yellow btn-header"><?php _e('Delivery', 'tisserie'); ?> <img src="<?php echo get_template_directory_uri(); ?>/images/icon-delivery.png" alt="icon delivery" class="img-fluid img-delivery"></a>
                        </div>
                        <div class="header-right col-xl-3 col-lg-3 col-md-12 col-sm-7 col-12 order-xl-3 order-lg-3 order-md-1 order-sm-1 order-1 d-xl-block d-lg-block d-md-block d-sm-none d-none">
                            <a href="<?php echo home_url('/delivery'); ?>" class="btn btn-md btn-yellow btn-header"><?php _e('Delivery', 'tisserie'); ?> <img src="<?php echo get_template_directory_uri(); ?>/images/icon-delivery.png" alt="icon delivery" class="img-fluid img-delivery"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>