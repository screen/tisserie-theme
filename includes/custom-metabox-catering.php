<?php

$arr_categories = array();
$categories = array();

$arr_categories = get_terms(array('taxonomy' => 'product_cat', 'hide_empty' => false, 'order' => 'ASC', 'orderby' => 'menu_order'));
foreach ($arr_categories as $item) {
    $categories[$item->term_id] = $item->name; 
}

/* --------------------------------------------------------------
    1.- DELIVERY: HERO SECTION
-------------------------------------------------------------- */
$cmb_catering_menu_pdf = new_cmb2_box(array(
    'id'            => $prefix . 'catering_pdf_metabox',
    'title'         => esc_html__('Catering: PDF para Descargar', 'tisserie'),
    'object_types'  => array('page'),
    'show_on'       => array('key' => 'page-template', 'value' => 'templates/page-catering.php'),
    'context'       => 'normal',
    'priority'      => 'high',
    'show_names'    => true,
    'cmb_styles'    => true,
    'closed'        => false
));

$cmb_catering_menu_pdf->add_field( array(
    'id'        => $prefix . 'store_pdf_download',
    'name'      => esc_html__( 'Archivo PDF para Descarga', 'tisserie' ),
    'desc'      => esc_html__( 'Inserte el archivo PDF de Catalogo para su descarga', 'tisserie' ),
    'type'    => 'file',
    'options' => array(
        'url' => false
    ),
    'text'    => array(
        'add_upload_file_text' => esc_html__( 'Cargar PDF', 'tisserie' ),
    ),
    'query_args' => array(
        'type' => array(
            'application/pdf'
        )
    ),
    'preview_size' => 'thumbnail'
));