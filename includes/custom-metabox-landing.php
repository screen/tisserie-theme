<?php
/* --------------------------------------------------------------
    1.- LANDING: HERO SECTION
-------------------------------------------------------------- */
$cmb_home_hero = new_cmb2_box( array(
    'id'            => $prefix . 'home_hero_metabox',
    'title'         => esc_html__( 'Landing: Hero Principal', 'tisserie' ),
    'object_types'  => array( 'page' ),
    'show_on'      => array( 'key' => 'page-template', 'value' => 'templates/page-landing.php' ),
    'context'    => 'normal',
    'priority'   => 'high',
    'show_names' => true,
    'cmb_styles' => true,
    'closed'     => false
) );

$cmb_home_hero->add_field( array(
    'id'   => $prefix . 'hero_banner_bg',
    'name'      => esc_html__( 'Imagen de Fondo del Hero', 'tisserie' ),
    'desc'      => esc_html__( 'Cargar un fondo para esta sección', 'tisserie' ),
    'type'    => 'file',

    'options' => array(
        'url' => false
    ),
    'text'    => array(
        'add_upload_file_text' => esc_html__( 'Cargar Imagen de Fondo', 'tisserie' ),
    ),
    'query_args' => array(
        'type' => array(
            'image/gif',
            'image/jpeg',
            'image/png'
        )
    ),
    'preview_size' => 'medium'
) );

$cmb_home_hero->add_field( array(
    'id'   => $prefix . 'hero_banner_logo',
    'name'      => esc_html__( 'Logo del Hero', 'tisserie' ),
    'desc'      => esc_html__( 'Cargar un logo para esta sección', 'tisserie' ),
    'type'    => 'file',

    'options' => array(
        'url' => false
    ),
    'text'    => array(
        'add_upload_file_text' => esc_html__( 'Cargar Logo', 'tisserie' ),
    ),
    'query_args' => array(
        'type' => array(
            'image/gif',
            'image/jpeg',
            'image/png'
        )
    ),
    'preview_size' => 'thumbnail'
) );


$cmb_home_hero->add_field( array(
    'id'        => $prefix . 'hero_banner_button_text',
    'name'      => esc_html__( 'Titulo del Boton', 'tisserie' ),
    'desc'      => esc_html__( 'Ingrese un texto descriptivo para el Botón', 'tisserie' ),
    'type'      => 'text'
) );

$cmb_home_hero->add_field( array(
    'id'        => $prefix . 'hero_banner_button_modal',
    'name'      => esc_html__( '¿Activar Modal?', 'tisserie' ),
    'desc'      => esc_html__( 'Acive si este botón tendra un link externo ó abriá un modalbox', 'tisserie' ),
    'type'      => 'checkbox'
) );

$cmb_home_hero->add_field( array(
    'id'        => $prefix . 'hero_banner_button_url',
    'name'      => esc_html__( 'Link URL del Boton', 'tisserie' ),
    'desc'      => esc_html__( 'Ingrese un URL de acción para el Botón', 'tisserie' ),
    'type'      => 'text_url'
) );


/* --------------------------------------------------------------
    2.- LANDING: TWO COLUMNS HERO SECTION
-------------------------------------------------------------- */
$cmb_home_columns = new_cmb2_box( array(
    'id'            => $prefix . 'home_columns_metabox',
    'title'         => esc_html__( 'Landing: Hero de Dos Columnas', 'tisserie' ),
    'object_types'  => array( 'page' ),
    'show_on'      => array( 'key' => 'page-template', 'value' => 'templates/page-landing.php' ),
    'context'    => 'normal',
    'priority'   => 'high',
    'show_names' => true,
    'cmb_styles' => true,
    'closed'     => false
) );

$cmb_home_columns->add_field( array(
    'id'   => $prefix . 'hero_column1_bg',
    'name'      => esc_html__( 'Imagen de Fondo de la Columna 1', 'tisserie' ),
    'desc'      => esc_html__( 'Cargar un fondo para esta sección', 'tisserie' ),
    'type'    => 'file',

    'options' => array(
        'url' => false
    ),
    'text'    => array(
        'add_upload_file_text' => esc_html__( 'Cargar Imagen de Fondo', 'tisserie' ),
    ),
    'query_args' => array(
        'type' => array(
            'image/gif',
            'image/jpeg',
            'image/png'
        )
    ),
    'preview_size' => 'medium'
) );


$cmb_home_columns->add_field( array(
    'id'        => $prefix . 'hero_column1_text',
    'name'      => esc_html__( 'Titulo de la Columna 1', 'tisserie' ),
    'desc'      => esc_html__( 'Ingrese un texto descriptivo para el Botón', 'tisserie' ),
    'type'      => 'text'
) );

$cmb_home_columns->add_field( array(
    'id'   => $prefix . 'hero_column2_bg',
    'name'      => esc_html__( 'Imagen de Fondo de la Columna 2', 'tisserie' ),
    'desc'      => esc_html__( 'Cargar un fondo para esta sección', 'tisserie' ),
    'type'    => 'file',

    'options' => array(
        'url' => false
    ),
    'text'    => array(
        'add_upload_file_text' => esc_html__( 'Cargar Imagen de Fondo', 'tisserie' ),
    ),
    'query_args' => array(
        'type' => array(
            'image/gif',
            'image/jpeg',
            'image/png'
        )
    ),
    'preview_size' => 'medium'
) );

$cmb_home_columns->add_field( array(
    'id'        => $prefix . 'hero_column2_text',
    'name'      => esc_html__( 'Texto de la Columna 2', 'tisserie' ),
    'desc'      => esc_html__( 'Ingrese un texto descriptivo para el Botón', 'tisserie' ),
    'type' => 'wysiwyg',
    'options' => array(
        'textarea_rows' => get_option('default_post_edit_rows', 2),
        'teeny' => false
    )
) );

/* --------------------------------------------------------------
    3.- LANDING: SUBSCRIBE BAR
-------------------------------------------------------------- */
$cmb_home_subscription = new_cmb2_box( array(
    'id'            => $prefix . 'home_subscribe_metabox',
    'title'         => esc_html__( 'Landing: Barra de Suscripción', 'tisserie' ),
    'object_types'  => array( 'page' ),
    'show_on'      => array( 'key' => 'page-template', 'value' => 'templates/page-landing.php' ),
    'context'    => 'normal',
    'priority'   => 'high',
    'show_names' => true,
    'cmb_styles' => true,
    'closed'     => false
) );

$cmb_home_subscription->add_field( array(
    'id'   => $prefix . 'home_subscription_icon',
    'name'      => esc_html__( 'Icono para Texto de la Barra', 'tisserie' ),
    'desc'      => esc_html__( 'Cargar un icono para esta sección', 'tisserie' ),
    'type'    => 'file',

    'options' => array(
        'url' => false
    ),
    'text'    => array(
        'add_upload_file_text' => esc_html__( 'Cargar Ícono', 'tisserie' ),
    ),
    'query_args' => array(
        'type' => array(
            'image/gif',
            'image/jpeg',
            'image/png'
        )
    ),
    'preview_size' => 'medium'
) );

$cmb_home_subscription->add_field( array(
    'id'        => $prefix . 'home_subscription_text',
    'name'      => esc_html__( 'Texto de la Barra', 'tisserie' ),
    'desc'      => esc_html__( 'Ingrese un texto descriptivo para la Barra', 'tisserie' ),
    'type'      => 'text'
) );

$cmb_home_subscription->add_field( array(
    'id'        => $prefix . 'home_subscription_button_text',
    'name'      => esc_html__( 'Texto del Botón', 'tisserie' ),
    'desc'      => esc_html__( 'Ingrese un texto descriptivo para el Botón', 'tisserie' ),
    'type'      => 'text'
) );

/* --------------------------------------------------------------
    4.- LANDING: ABOUT SECTION
-------------------------------------------------------------- */
$cmb_home_about = new_cmb2_box( array(
    'id'            => $prefix . 'home_about_metabox',
    'title'         => esc_html__( 'Landing: Sección Nosotros', 'tisserie' ),
    'object_types'  => array( 'page' ),
    'show_on'      => array( 'key' => 'page-template', 'value' => 'templates/page-landing.php' ),
    'context'    => 'normal',
    'priority'   => 'high',
    'show_names' => true,
    'cmb_styles' => true,
    'closed'     => false
) );

$cmb_home_about->add_field( array(
    'id'   => $prefix . 'home_about_image',
    'name'      => esc_html__( 'Imagen de Sección', 'tisserie' ),
    'desc'      => esc_html__( 'Cargar una imagen para esta sección', 'tisserie' ),
    'type'    => 'file',

    'options' => array(
        'url' => false
    ),
    'text'    => array(
        'add_upload_file_text' => esc_html__( 'Cargar Imagen', 'tisserie' ),
    ),
    'query_args' => array(
        'type' => array(
            'image/gif',
            'image/jpeg',
            'image/png'
        )
    ),
    'preview_size' => 'medium'
) );

$cmb_home_about->add_field( array(
    'id'        => $prefix . 'home_about_main_text',
    'name'      => esc_html__( 'Texto principal de la Sección', 'tisserie' ),
    'desc'      => esc_html__( 'Ingrese un texto descriptivo para la sección', 'tisserie' ),
    'type' => 'wysiwyg',
    'options' => array(
        'textarea_rows' => get_option('default_post_edit_rows', 2),
        'teeny' => false
    )
) );

$cmb_home_about->add_field( array(
    'id'        => $prefix . 'home_about_second_text',
    'name'      => esc_html__( 'Texto secondario de la Sección', 'tisserie' ),
    'desc'      => esc_html__( 'Ingrese un texto descriptivo luego del separador', 'tisserie' ),
    'type' => 'wysiwyg',
    'options' => array(
        'textarea_rows' => get_option('default_post_edit_rows', 2),
        'teeny' => false
    )
) );

$cmb_home_about->add_field( array(
    'id'   => $prefix . 'home_about_icon',
    'name'      => esc_html__( 'Iconos de Sección', 'tisserie' ),
    'desc'      => esc_html__( 'Cargar un icono para el final de esta sección', 'tisserie' ),
    'type'          => 'file_list',
    'query_args'    => array( 'type' => 'image' ),
    'text' => array(
        'add_upload_files_text'     => esc_html__( 'Cargar icono', 'tisserie' ),
        'remove_image_text'         => esc_html__( 'Remove icono', 'tisserie' ),
        'file_text'                 => esc_html__( 'icono', 'tisserie' ),
        'file_download_text'        => esc_html__( 'Descargar', 'tisserie' ),
        'remove_text'               => esc_html__( 'Remover', 'tisserie' ),
    ),
    'preview_size'  => 'thumbnail'
) );

/* --------------------------------------------------------------
    5.- LANDING: PRODUCTS SECTION
-------------------------------------------------------------- */
$cmb_home_products = new_cmb2_box( array(
    'id'            => $prefix . 'home_products_metabox',
    'title'         => esc_html__( 'Landing: Sección Productos', 'tisserie' ),
    'object_types'  => array( 'page' ),
    'show_on'      => array( 'key' => 'page-template', 'value' => 'templates/page-landing.php' ),
    'context'    => 'normal',
    'priority'   => 'high',
    'show_names' => true,
    'cmb_styles' => true,
    'closed'     => false
) );


$cmb_home_products->add_field( array(
    'id'        => $prefix . 'home_products_title',
    'name'      => esc_html__( 'Título de la Sección', 'tisserie' ),
    'desc'      => esc_html__( 'Ingrese un título descriptivo para la Sección', 'tisserie' ),
    'type'      => 'text'
) );


/* --------------------------------------------------------------
    6.- LANDING: TESTIMONIALS SECTION
-------------------------------------------------------------- */
$cmb_home_testimonials = new_cmb2_box( array(
    'id'            => $prefix . 'home_testimonials_metabox',
    'title'         => esc_html__( 'Landing: Sección Testimonios', 'tisserie' ),
    'object_types'  => array( 'page' ),
    'show_on'      => array( 'key' => 'page-template', 'value' => 'templates/page-landing.php' ),
    'context'    => 'normal',
    'priority'   => 'high',
    'show_names' => true,
    'cmb_styles' => true,
    'closed'     => false
) );

$cmb_home_testimonials->add_field( array(
    'id'        => $prefix . 'home_testimonials_title',
    'name'      => esc_html__( 'Título de la Sección', 'tisserie' ),
    'desc'      => esc_html__( 'Ingrese un título descriptivo para la Sección', 'tisserie' ),
    'type'      => 'text'
) );

$group_field_id = $cmb_home_testimonials->add_field( array(
    'id'          => $prefix . 'home_testimonials_group',
    'name'      => esc_html__( 'Grupos de Testimonios', 'tisserie' ),
    'description' => __( 'Testimonios dentro de la Sección', 'tisserie' ),
    'type'        => 'group',
    'options'     => array(
        'group_title'       => __( 'Testimonio {#}', 'tisserie' ),
        'add_button'        => __( 'Agregar otro Testimonio', 'tisserie' ),
        'remove_button'     => __( 'Remover Testimonio', 'tisserie' ),
        'sortable'          => true,
        'closed'         => true,
        'remove_confirm' => esc_html__( '¿Estas seguro de remover este Testimonio?', 'tisserie' )
    )
) );

$cmb_home_testimonials->add_group_field( $group_field_id, array(
    'id'   => 'logo',
    'name'      => esc_html__( 'Logo del Testimonio', 'tisserie' ),
    'desc'      => esc_html__( 'Cargar un logo para este Testimonio', 'tisserie' ),
    'type'    => 'file',

    'options' => array(
        'url' => false
    ),
    'text'    => array(
        'add_upload_file_text' => esc_html__( 'Cargar Logo', 'tisserie' ),
    ),
    'query_args' => array(
        'type' => array(
            'image/gif',
            'image/jpeg',
            'image/png'
        )
    ),
    'preview_size' => 'thumbnail'
) );

$cmb_home_testimonials->add_group_field( $group_field_id, array(
    'id'        => 'author',
    'name'      => esc_html__( 'Autor del Testimonio', 'tisserie' ),
    'desc'      => esc_html__( 'Ingrese el nombre del autor del testomonio', 'tisserie' ),
    'type' => 'text'
) );

$cmb_home_testimonials->add_group_field( $group_field_id, array(
    'id'        => 'desc',
    'name'      => esc_html__( 'Descripción del Testimonio', 'tisserie' ),
    'desc'      => esc_html__( 'Ingrese una descripción alusiva del Testimonio', 'tisserie' ),
    'type' => 'wysiwyg',
    'options' => array(
        'textarea_rows' => get_option('default_post_edit_rows', 2),
        'teeny' => false
    )
) );

/* --------------------------------------------------------------
    7.- LANDING: GALLERY SECTION
-------------------------------------------------------------- */
$cmb_home_gallery = new_cmb2_box( array(
    'id'            => $prefix . 'home_gallery_metabox',
    'title'         => esc_html__( 'Landing: Sección Galería', 'tisserie' ),
    'object_types'  => array( 'page' ),
    'show_on'      => array( 'key' => 'page-template', 'value' => 'templates/page-landing.php' ),
    'context'    => 'normal',
    'priority'   => 'high',
    'show_names' => true,
    'cmb_styles' => true,
    'closed'     => false
) );

$cmb_home_gallery->add_field( array(
    'id'            => $prefix . 'home_gallery_list',
    'name'          => esc_html__('Galería', 'tisserie'),
    'desc'          => esc_html__('Cargue o seleccione las imágenes para incluirlas en la Galería', 'tisserie'),
    'type'          => 'file_list',
    'query_args'    => array( 'type' => 'image' ),
    'text' => array(
        'add_upload_files_text'     => esc_html__( 'Cargar Imágenes', 'tisserie' ),
        'remove_image_text'         => esc_html__( 'Remove Imágenes', 'tisserie' ),
        'file_text'                 => esc_html__( 'Imagen', 'tisserie' ),
        'file_download_text'        => esc_html__( 'Descargar', 'tisserie' ),
        'remove_text'               => esc_html__( 'Remover', 'tisserie' ),
    ),
    'preview_size'  => 'thumbnail'
) );
