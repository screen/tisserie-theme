<?php
/* --------------------------------------------------------------
    1.- CONTACT: HERO SECTION
-------------------------------------------------------------- */
$cmb_contact_hero = new_cmb2_box(array(
    'id'            => $prefix . 'contact_hero_metabox',
    'title'         => esc_html__('Contacto: Hero Principal', 'tisserie'),
    'object_types'  => array('page'),
    'show_on'       => array('key' => 'page-template', 'value' => 'templates/page-contact.php'),
    'context'       => 'normal',
    'priority'      => 'high',
    'show_names'    => true,
    'cmb_styles'    => true,
    'closed'        => false
));

$cmb_contact_hero->add_field( array(
    'id'        => $prefix . 'contact_hero_bg',
    'name'      => esc_html__('Imagen de Fondo del Hero', 'tisserie'),
    'desc'      => esc_html__('Cargar un fondo para este Hero', 'tisserie'),
    'type'      => 'file',

    'options'   => array(
        'url'   => false
    ),
    'text'      => array(
        'add_upload_file_text' => esc_html__('Cargar fondo', 'tisserie'),
    ),
    'query_args' => array(
        'type'   => array(
            'image/gif',
            'image/jpeg',
            'image/png'
        )
    ),
    'preview_size' => 'thumbnail'
));