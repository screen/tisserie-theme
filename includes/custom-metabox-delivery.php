<?php
/* --------------------------------------------------------------
    1.- DELIVERY: HERO SECTION
-------------------------------------------------------------- */
$cmb_delivery_hero = new_cmb2_box(array(
    'id'            => $prefix . 'delivery_hero_metabox',
    'title'         => esc_html__('About: Hero Principal', 'tisserie'),
    'object_types'  => array('page'),
    'show_on'       => array('key' => 'page-template', 'value' => 'templates/page-delivery.php'),
    'context'       => 'normal',
    'priority'      => 'high',
    'show_names'    => true,
    'cmb_styles'    => true,
    'closed'        => false
));

$cmb_delivery_hero->add_field( array(
    'id'        => $prefix . 'delivery_hero_bg',
    'name'      => esc_html__('Imagen de Fondo del Hero', 'tisserie'),
    'desc'      => esc_html__('Cargar un fondo para este Hero', 'tisserie'),
    'type'      => 'file',

    'options'   => array(
        'url'   => false
    ),
    'text'      => array(
        'add_upload_file_text' => esc_html__('Cargar fondo', 'tisserie'),
    ),
    'query_args' => array(
        'type'   => array(
            'image/gif',
            'image/jpeg',
            'image/png'
        )
    ),
    'preview_size' => 'thumbnail'
));

$cmb_delivery_hero->add_field( array(
    'id'        => $prefix . 'delivery_hero_title',
    'name'      => esc_html__('Título del Hero', 'tisserie'),
    'desc'      => esc_html__('Ingrese el Título del Hero', 'tisserie'),
    'type'      => 'text'
));


$cmb_delivery_hero->add_field( array(
    'id'        => $prefix . 'delivery_hero_desc',
    'name'      => esc_html__('Descripción del Hero', 'tisserie'),
    'desc'      => esc_html__('Ingrese la descripción del Hero', 'tisserie'),
    'type'      => 'wysiwyg',
    'options'   => array(
        'textarea_rows' => get_option('default_post_edit_rows', 2),
        'teeny' => false
    )
));

$group_field_id = $cmb_delivery_hero->add_field( array(
    'id'          => $prefix . 'delivery_hero_logo_list',
    'name'      => esc_html__( 'Grupos de Logo', 'tisserie' ),
    'description' => __( 'Logo dentro de la Sección', 'tisserie' ),
    'type'        => 'group',
    'options'     => array(
        'group_title'       => __( 'Logo {#}', 'tisserie' ),
        'add_button'        => __( 'Agregar otro Logo', 'tisserie' ),
        'remove_button'     => __( 'Remover Logo', 'tisserie' ),
        'sortable'          => true,
        'closed'         => true,
        'remove_confirm' => esc_html__( '¿Estas seguro de remover este Logo?', 'tisserie' )
    )
) );

$cmb_delivery_hero->add_group_field( $group_field_id, array(
    'id'   => 'logo',
    'name'      => esc_html__( 'Logo del App', 'tisserie' ),
    'desc'      => esc_html__( 'Cargar un logo para esta App', 'tisserie' ),
    'type'    => 'file',

    'options' => array(
        'url' => false
    ),
    'text'    => array(
        'add_upload_file_text' => esc_html__( 'Cargar Logo', 'tisserie' ),
    ),
    'query_args' => array(
        'type' => array(
            'image/gif',
            'image/jpeg',
            'image/png'
        )
    ),
    'preview_size' => 'thumbnail'
) );

$cmb_delivery_hero->add_group_field( $group_field_id, array(
    'id'        => 'link',
    'name'      => esc_html__( 'Link del App', 'tisserie' ),
    'desc'      => esc_html__( 'Ingrese el link del app del logo', 'tisserie' ),
    'type' => 'text_url'
) );