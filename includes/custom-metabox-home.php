<?php
/* --------------------------------------------------------------
    1.- HOME: SLIDER SECTION
-------------------------------------------------------------- */
$cmb_home_slider = new_cmb2_box( array(
    'id'            => $prefix . 'home_slider_metabox',
    'title'         => esc_html__( 'Home: Slider Principal', 'tisserie' ),
    'object_types'  => array( 'page' ),
    'show_on'      => array( 'key' => 'page-template', 'value' => 'templates/page-home.php' ),
    'context'    => 'normal',
    'priority'   => 'high',
    'show_names' => true,
    'cmb_styles' => true,
    'closed'     => false
) );

$group_field_id = $cmb_home_slider->add_field( array(
    'id'          => $prefix . 'home_slider_group',
    'name'      => esc_html__( 'Grupos de Slides', 'tisserie' ),
    'description' => __( 'Slides dentro del Slider', 'tisserie' ),
    'type'        => 'group',
    'options'     => array(
        'group_title'       => __( 'Slide {#}', 'tisserie' ),
        'add_button'        => __( 'Agregar otro Slide', 'tisserie' ),
        'remove_button'     => __( 'Remover Slide', 'tisserie' ),
        'sortable'          => true,
        'closed'         => true,
        'remove_confirm' => esc_html__( '¿Estas seguro de remover este Slide?', 'tisserie' )
    )
) );

$cmb_home_slider->add_group_field( $group_field_id, array(
    'id'   => 'bg_image',
    'name'      => esc_html__( 'Imagen de Fondo del Slide', 'tisserie' ),
    'desc'      => esc_html__( 'Cargar un fondo para este Slide', 'tisserie' ),
    'type'    => 'file',

    'options' => array(
        'url' => false
    ),
    'text'    => array(
        'add_upload_file_text' => esc_html__( 'Cargar fondo', 'tisserie' ),
    ),
    'query_args' => array(
        'type' => array(
            'image/gif',
            'image/jpeg',
            'image/png'
        )
    ),
    'preview_size' => 'thumbnail'
) );

$cmb_home_slider->add_group_field( $group_field_id, array(
    'id'        => 'title',
    'name'      => esc_html__( 'Título del Slide', 'tisserie' ),
    'desc'      => esc_html__( 'Ingrese el Título del Slide', 'tisserie' ),
    'type' => 'text'
) );


$cmb_home_slider->add_group_field( $group_field_id, array(
    'id'        => 'desc',
    'name'      => esc_html__( 'Descripción del Slide', 'tisserie' ),
    'desc'      => esc_html__( 'Ingrese la descripción del Slide', 'tisserie' ),
    'type' => 'wysiwyg',
    'options' => array(
        'textarea_rows' => get_option('default_post_edit_rows', 2),
        'teeny' => false
    )
) );

$cmb_home_slider->add_group_field( $group_field_id, array(
    'id'        => 'button_text',
    'name'      => esc_html__( 'Texto del Boton en Slide', 'tisserie' ),
    'desc'      => esc_html__( 'Ingrese el Texto del boton correspondiente', 'tisserie' ),
    'type' => 'text'
) );

$cmb_home_slider->add_group_field( $group_field_id, array(
    'id'        => 'button_link',
    'name'      => esc_html__( 'Link del Boton en Slide', 'tisserie' ),
    'desc'      => esc_html__( 'Ingrese el link de accion del boton correspondiente', 'tisserie' ),
    'type' => 'text'
) );

/* --------------------------------------------------------------
    2.- HOME: SECOND SECTION
-------------------------------------------------------------- */

$cmb_home_second_section = new_cmb2_box( array(
    'id'            => $prefix . 'home_second_section_metabox',
    'title'         => esc_html__( 'Home: Sección About', 'tisserie' ),
    'object_types'  => array( 'page' ),
    'show_on'      => array( 'key' => 'page-template', 'value' => 'templates/page-home.php' ),
    'context'    => 'normal',
    'priority'   => 'high',
    'show_names' => true,
    'cmb_styles' => true,
    'closed'     => false
) );

$cmb_home_second_section->add_field( array(
    'id'   => $prefix . 'home_second_section_bg',
    'name'      => esc_html__( 'Imagen de Fondo de la Sección', 'tisserie' ),
    'desc'      => esc_html__( 'Cargar un fondo para esta sección', 'tisserie' ),
    'type'    => 'file',

    'options' => array(
        'url' => false
    ),
    'text'    => array(
        'add_upload_file_text' => esc_html__( 'Cargar Imagen de Fondo', 'tisserie' ),
    ),
    'query_args' => array(
        'type' => array(
            'image/gif',
            'image/jpeg',
            'image/png'
        )
    ),
    'preview_size' => 'medium'
) );

$cmb_home_second_section->add_field( array(
    'id'   => $prefix . 'home_second_section_title',
    'name'      => esc_html__( 'Título de la Sección', 'tisserie' ),
    'desc'      => esc_html__( 'Ingrese el Título de la Sección', 'tisserie' ),
    'type' => 'text'
) );

$cmb_home_second_section->add_field( array(
    'id'   => $prefix . 'home_second_section_desc',
    'name'      => esc_html__( 'Descripción de la Sección', 'tisserie' ),
    'desc'      => esc_html__( 'Ingrese la Descripción de la Sección', 'tisserie' ),
    'type' => 'wysiwyg',
    'options' => array(
        'textarea_rows' => get_option('default_post_edit_rows', 2),
        'teeny' => false
    )
) );

$cmb_home_second_section->add_field( array(
    'id'   => $prefix . 'home_second_section_image',
    'name'      => esc_html__( 'Imagen Principal de la Sección', 'tisserie' ),
    'desc'      => esc_html__( 'Cargar una imagen para esta sección', 'tisserie' ),
    'type'    => 'file',

    'options' => array(
        'url' => false
    ),
    'text'    => array(
        'add_upload_file_text' => esc_html__( 'Cargar Imagen Principal', 'tisserie' ),
    ),
    'query_args' => array(
        'type' => array(
            'image/gif',
            'image/jpeg',
            'image/png'
        )
    ),
    'preview_size' => 'medium'
) );

$cmb_home_second_section->add_field( array(
    'id'            => $prefix . 'home_second_section_logos',
    'name'          => esc_html__('Logos', 'tisserie'),
    'desc'          => esc_html__('Cargue o seleccione los logos para incluirlas en la Galería', 'tisserie'),
    'type'          => 'file_list',
    'query_args'    => array( 'type' => 'image' ),
    'text' => array(
        'add_upload_files_text'     => esc_html__( 'Cargar Logo', 'tisserie' ),
        'remove_image_text'         => esc_html__( 'Remover Logo', 'tisserie' ),
        'file_text'                 => esc_html__( 'Logo', 'tisserie' ),
        'file_download_text'        => esc_html__( 'Descargar', 'tisserie' ),
        'remove_text'               => esc_html__( 'Remover', 'tisserie' ),
    ),
    'preview_size'  => 'thumbnail'
) );

/* --------------------------------------------------------------
    3.- HOME: MENU SECTION
-------------------------------------------------------------- */

$cmb_home_menu_section = new_cmb2_box( array(
    'id'            => $prefix . 'home_menu_metabox',
    'title'         => esc_html__( 'Home: Sección Menu', 'tisserie' ),
    'object_types'  => array( 'page' ),
    'show_on'      => array( 'key' => 'page-template', 'value' => 'templates/page-home.php' ),
    'context'    => 'normal',
    'priority'   => 'high',
    'show_names' => true,
    'cmb_styles' => true,
    'closed'     => false
) );

$cmb_home_menu_section->add_field( array(
    'id'   => $prefix . 'home_menu_section_bg',
    'name'      => esc_html__( 'Imagen de Fondo de la Sección', 'tisserie' ),
    'desc'      => esc_html__( 'Cargar un fondo para esta sección', 'tisserie' ),
    'type'    => 'file',

    'options' => array(
        'url' => false
    ),
    'text'    => array(
        'add_upload_file_text' => esc_html__( 'Cargar Imagen de Fondo', 'tisserie' ),
    ),
    'query_args' => array(
        'type' => array(
            'image/gif',
            'image/jpeg',
            'image/png'
        )
    ),
    'preview_size' => 'medium'
) );

$cmb_home_menu_section->add_field( array(
    'id'            => $prefix . 'home_menu_section_images',
    'name'          => esc_html__('Imágenes flotantes', 'tisserie'),
    'desc'          => esc_html__('Cargue o seleccione las Imágenes para incluirlas en la Galería', 'tisserie'),
    'type'          => 'file_list',
    'query_args'    => array( 'type' => 'image' ),
    'text' => array(
        'add_upload_files_text'     => esc_html__( 'Cargar Imagen', 'tisserie' ),
        'remove_image_text'         => esc_html__( 'Remover Imagen', 'tisserie' ),
        'file_text'                 => esc_html__( 'Imagen', 'tisserie' ),
        'file_download_text'        => esc_html__( 'Descargar', 'tisserie' ),
        'remove_text'               => esc_html__( 'Remover', 'tisserie' ),
    ),
    'preview_size'  => 'thumbnail'
) );

$cmb_home_menu_section->add_field( array(
    'id'   => $prefix . 'home_menu_section_title',
    'name'      => esc_html__( 'Título de la Sección', 'tisserie' ),
    'desc'      => esc_html__( 'Ingrese el Título de la Sección', 'tisserie' ),
    'type' => 'text'
) );

$cmb_home_menu_section->add_field( array(
    'id'   => $prefix . 'home_menu_section_desc',
    'name'      => esc_html__( 'Descripción de la Sección', 'tisserie' ),
    'desc'      => esc_html__( 'Ingrese la Descripción de la Sección', 'tisserie' ),
    'type' => 'wysiwyg',
    'options' => array(
        'textarea_rows' => get_option('default_post_edit_rows', 2),
        'teeny' => false
    )
) );

$cmb_home_menu_section->add_field( array(
    'id'   => $prefix . 'home_menu_section_btn_text',
    'name'      => esc_html__( 'Texto del Botón', 'tisserie' ),
    'desc'      => esc_html__( 'Ingrese el Texto del Botón', 'tisserie' ),
    'type' => 'text'
) );

$cmb_home_menu_section->add_field( array(
    'id'   => $prefix . 'home_menu_section_btn_link',
    'name'      => esc_html__( 'Acción del Botón', 'tisserie' ),
    'desc'      => esc_html__( 'Ingrese la Acción del Botón', 'tisserie' ),
    'type' => 'text_url'
) );


/* --------------------------------------------------------------
    3.- HOME: MENU SECTION
-------------------------------------------------------------- */

$cmb_home_benefits_section = new_cmb2_box( array(
    'id'            => $prefix . 'home_benefits_metabox',
    'title'         => esc_html__( 'Home: Sección Menu', 'tisserie' ),
    'object_types'  => array( 'page' ),
    'show_on'      => array( 'key' => 'page-template', 'value' => 'templates/page-home.php' ),
    'context'    => 'normal',
    'priority'   => 'high',
    'show_names' => true,
    'cmb_styles' => true,
    'closed'     => false
) );

$cmb_home_benefits_section->add_field( array(
    'id'   => $prefix . 'home_benefits_section_title',
    'name'      => esc_html__( 'Título de la Sección', 'tisserie' ),
    'desc'      => esc_html__( 'Ingrese el Título de la Sección', 'tisserie' ),
    'type' => 'text'
) );

$cmb_home_benefits_section->add_field( array(
    'id'   => $prefix . 'home_benefits_section_desc',
    'name'      => esc_html__( 'Descripción de la Sección', 'tisserie' ),
    'desc'      => esc_html__( 'Ingrese la Descripción de la Sección', 'tisserie' ),
    'type' => 'wysiwyg',
    'options' => array(
        'textarea_rows' => get_option('default_post_edit_rows', 2),
        'teeny' => false
    )
) );

$group_field_id = $cmb_home_benefits_section->add_field( array(
    'id'          => $prefix . 'home_benefits_group',
    'name'      => esc_html__( 'Grupos de Beneficios', 'tisserie' ),
    'description' => __( 'Items dentro de la Sección', 'tisserie' ),
    'type'        => 'group',
    'options'     => array(
        'group_title'       => __( 'Beneficio {#}', 'tisserie' ),
        'add_button'        => __( 'Agregar otro Beneficio', 'tisserie' ),
        'remove_button'     => __( 'Remover Beneficio', 'tisserie' ),
        'sortable'          => true,
        'closed'         => true,
        'remove_confirm' => esc_html__( '¿Estas seguro de remover este Beneficio?', 'tisserie' )
    )
) );

$cmb_home_benefits_section->add_group_field( $group_field_id, array(
    'id'   => 'bg_image',
    'name'      => esc_html__( 'Ícono del Beneficio', 'tisserie' ),
    'desc'      => esc_html__( 'Cargar un ícono para este Beneficio', 'tisserie' ),
    'type'    => 'file',

    'options' => array(
        'url' => false
    ),
    'text'    => array(
        'add_upload_file_text' => esc_html__( 'Cargar ícono', 'tisserie' ),
    ),
    'query_args' => array(
        'type' => array(
            'image/gif',
            'image/jpeg',
            'image/png'
        )
    ),
    'preview_size' => 'thumbnail'
) );

$cmb_home_benefits_section->add_group_field( $group_field_id, array(
    'id'        => 'title',
    'name'      => esc_html__( 'Título del Beneficio', 'tisserie' ),
    'desc'      => esc_html__( 'Ingrese el Título del Beneficio', 'tisserie' ),
    'type' => 'text'
) );

/* --------------------------------------------------------------
    4.- HOME: TESTIMONIALS SECTION
-------------------------------------------------------------- */

$cmb_home_test_section = new_cmb2_box( array(
    'id'            => $prefix . 'home_test_metabox',
    'title'         => esc_html__( 'Home: Sección Testimoniales', 'tisserie' ),
    'object_types'  => array( 'page' ),
    'show_on'      => array( 'key' => 'page-template', 'value' => 'templates/page-home.php' ),
    'context'    => 'normal',
    'priority'   => 'high',
    'show_names' => true,
    'cmb_styles' => true,
    'closed'     => false
) );

$cmb_home_test_section->add_field( array(
    'id'   => $prefix . 'home_test_section_title',
    'name'      => esc_html__( 'Título de la Sección', 'tisserie' ),
    'desc'      => esc_html__( 'Ingrese el Título de la Sección', 'tisserie' ),
    'type' => 'text'
) );

$group_field_id = $cmb_home_test_section->add_field( array(
    'id'          => $prefix . 'home_test_group',
    'name'      => esc_html__( 'Grupos de Testimonios', 'tisserie' ),
    'description' => __( 'Testimonios dentro de la Sección', 'tisserie' ),
    'type'        => 'group',
    'options'     => array(
        'group_title'       => __( 'Testimonio {#}', 'tisserie' ),
        'add_button'        => __( 'Agregar otro Testimonio', 'tisserie' ),
        'remove_button'     => __( 'Remover Testimonio', 'tisserie' ),
        'sortable'          => true,
        'closed'         => true,
        'remove_confirm' => esc_html__( '¿Estas seguro de remover este Testimonio?', 'tisserie' )
    )
) );

$cmb_home_test_section->add_group_field( $group_field_id, array(
    'id'   => 'image',
    'name'      => esc_html__( 'Imagen del Testimonio', 'tisserie' ),
    'desc'      => esc_html__( 'Cargar una Imagen para este Testimonio', 'tisserie' ),
    'type'    => 'file',

    'options' => array(
        'url' => false
    ),
    'text'    => array(
        'add_upload_file_text' => esc_html__( 'Cargar Imagen', 'tisserie' ),
    ),
    'query_args' => array(
        'type' => array(
            'image/gif',
            'image/jpeg',
            'image/png'
        )
    ),
    'preview_size' => 'thumbnail'
) );

$cmb_home_test_section->add_group_field( $group_field_id, array(
    'id'        => 'author',
    'name'      => esc_html__( 'Autor del Testimonio', 'tisserie' ),
    'desc'      => esc_html__( 'Ingrese el nombre del autor del testomonio', 'tisserie' ),
    'type' => 'text'
) );

$cmb_home_test_section->add_group_field( $group_field_id, array(
    'id'        => 'desc',
    'name'      => esc_html__( 'Descripción del Testimonio', 'tisserie' ),
    'desc'      => esc_html__( 'Ingrese una descripción alusiva del Testimonio', 'tisserie' ),
    'type' => 'wysiwyg',
    'options' => array(
        'textarea_rows' => get_option('default_post_edit_rows', 2),
        'teeny' => false
    )
) );


/* --------------------------------------------------------------
    5.- HOME: DESCANSO SECTION
-------------------------------------------------------------- */

$cmb_home_descanso_section = new_cmb2_box( array(
    'id'            => $prefix . 'home_descanso_metabox',
    'title'         => esc_html__( 'Home: Hero Final', 'tisserie' ),
    'object_types'  => array( 'page' ),
    'show_on'      => array( 'key' => 'page-template', 'value' => 'templates/page-home.php' ),
    'context'    => 'normal',
    'priority'   => 'high',
    'show_names' => true,
    'cmb_styles' => true,
    'closed'     => false
) );

$cmb_home_descanso_section->add_field( array(
    'id'   => $prefix . 'home_descanso_section_bg',
    'name'      => esc_html__( 'Imagen de Fondo de la Sección', 'tisserie' ),
    'desc'      => esc_html__( 'Cargar un fondo para esta sección', 'tisserie' ),
    'type'    => 'file',

    'options' => array(
        'url' => false
    ),
    'text'    => array(
        'add_upload_file_text' => esc_html__( 'Cargar Imagen de Fondo', 'tisserie' ),
    ),
    'query_args' => array(
        'type' => array(
            'image/gif',
            'image/jpeg',
            'image/png'
        )
    ),
    'preview_size' => 'medium'
) );

$cmb_home_descanso_section->add_field( array(
    'id'   => $prefix . 'home_descanso_section_title',
    'name'      => esc_html__( 'Título de la Sección', 'tisserie' ),
    'desc'      => esc_html__( 'Ingrese el Título de la Sección', 'tisserie' ),
    'type' => 'text'
) );

$cmb_home_descanso_section->add_field( array(
    'id'   => $prefix . 'home_descanso_section_btn_text',
    'name'      => esc_html__( 'Texto del Botón', 'tisserie' ),
    'desc'      => esc_html__( 'Ingrese el Texto del Botón', 'tisserie' ),
    'type' => 'text'
) );

$cmb_home_descanso_section->add_field( array(
    'id'   => $prefix . 'home_descanso_section_btn_link',
    'name'      => esc_html__( 'Acción del Botón', 'tisserie' ),
    'desc'      => esc_html__( 'Ingrese la Acción del Botón', 'tisserie' ),
    'type' => 'text_url'
) );

/* --------------------------------------------------------------
    6.- HOME: GALERÍA SECTION
-------------------------------------------------------------- */

$cmb_home_footer_gallery_section = new_cmb2_box( array(
    'id'            => $prefix . 'home_footer_gallery_metabox',
    'title'         => esc_html__( 'Home: Galería', 'tisserie' ),
    'object_types'  => array( 'page' ),
    'show_on'      => array( 'key' => 'page-template', 'value' => 'templates/page-home.php' ),
    'context'    => 'normal',
    'priority'   => 'high',
    'show_names' => true,
    'cmb_styles' => true,
    'closed'     => false
) );

$cmb_home_footer_gallery_section->add_field( array(
    'id'            => $prefix . 'home_footer_gallery_list',
    'name'          => esc_html__('Galería', 'tisserie'),
    'desc'          => esc_html__('Cargue o seleccione las imágenes para incluirlas en la Galería', 'tisserie'),
    'type'          => 'file_list',
    'query_args'    => array( 'type' => 'image' ),
    'text' => array(
        'add_upload_files_text'     => esc_html__( 'Cargar Imágenes', 'tisserie' ),
        'remove_image_text'         => esc_html__( 'Remove Imágenes', 'tisserie' ),
        'file_text'                 => esc_html__( 'Imagen', 'tisserie' ),
        'file_download_text'        => esc_html__( 'Descargar', 'tisserie' ),
        'remove_text'               => esc_html__( 'Remover', 'tisserie' ),
    ),
    'preview_size'  => 'thumbnail'
) );
