<?php
/* --------------------------------------------------------------
    1.- THANKS: HERO SECTION
-------------------------------------------------------------- */
$cmb_home_hero = new_cmb2_box( array(
    'id'            => $prefix . 'thanks_hero_metabox',
    'title'         => esc_html__( 'Thanks: Hero Principal', 'tisserie' ),
    'object_types'  => array( 'page' ),
    'show_on'      => array( 'key' => 'page-template', 'value' => 'templates/page-thanks.php' ),
    'context'    => 'normal',
    'priority'   => 'high',
    'show_names' => true,
    'cmb_styles' => true,
    'closed'     => false
) );

$cmb_home_hero->add_field( array(
    'id'   => $prefix . 'thanks_banner_bg',
    'name'      => esc_html__( 'Imagen de Fondo del Hero', 'tisserie' ),
    'desc'      => esc_html__( 'Cargar un fondo para esta sección', 'tisserie' ),
    'type'    => 'file',

    'options' => array(
        'url' => false
    ),
    'text'    => array(
        'add_upload_file_text' => esc_html__( 'Cargar Imagen de Fondo', 'tisserie' ),
    ),
    'query_args' => array(
        'type' => array(
            'image/gif',
            'image/jpeg',
            'image/png'
        )
    ),
    'preview_size' => 'medium'
) );

$cmb_home_hero->add_field( array(
    'id'   => $prefix . 'thanks_banner_logo',
    'name'      => esc_html__( 'Logo del Hero', 'tisserie' ),
    'desc'      => esc_html__( 'Cargar un logo para esta sección', 'tisserie' ),
    'type'    => 'file',

    'options' => array(
        'url' => false
    ),
    'text'    => array(
        'add_upload_file_text' => esc_html__( 'Cargar Logo', 'tisserie' ),
    ),
    'query_args' => array(
        'type' => array(
            'image/gif',
            'image/jpeg',
            'image/png'
        )
    ),
    'preview_size' => 'thumbnail'
) );
