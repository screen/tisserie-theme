<?php
/* --------------------------------------------------------------
    1.- ABOUT: HERO SECTION
-------------------------------------------------------------- */
$cmb_about_hero = new_cmb2_box(array(
    'id'            => $prefix . 'about_hero_metabox',
    'title'         => esc_html__('About: Hero Principal', 'tisserie'),
    'object_types'  => array('page'),
    'show_on'       => array('key' => 'page-template', 'value' => 'templates/page-about.php'),
    'context'       => 'normal',
    'priority'      => 'high',
    'show_names'    => true,
    'cmb_styles'    => true,
    'closed'        => false
));

$cmb_about_hero->add_field( array(
    'id'        => $prefix . 'about_hero_bg',
    'name'      => esc_html__('Imagen de Fondo del Hero', 'tisserie'),
    'desc'      => esc_html__('Cargar un fondo para este Hero', 'tisserie'),
    'type'      => 'file',

    'options'   => array(
        'url'   => false
    ),
    'text'      => array(
        'add_upload_file_text' => esc_html__('Cargar fondo', 'tisserie'),
    ),
    'query_args' => array(
        'type'   => array(
            'image/gif',
            'image/jpeg',
            'image/png'
        )
    ),
    'preview_size' => 'thumbnail'
));

$cmb_about_hero->add_field( array(
    'id'        => $prefix . 'about_hero_title',
    'name'      => esc_html__('Título del Hero', 'tisserie'),
    'desc'      => esc_html__('Ingrese el Título del Hero', 'tisserie'),
    'type'      => 'text'
));


$cmb_about_hero->add_field( array(
    'id'        => $prefix . 'about_hero_desc',
    'name'      => esc_html__('Descripción del Hero', 'tisserie'),
    'desc'      => esc_html__('Ingrese la descripción del Hero', 'tisserie'),
    'type'      => 'wysiwyg',
    'options'   => array(
        'textarea_rows' => get_option('default_post_edit_rows', 2),
        'teeny' => false
    )
));

$cmb_about_hero->add_field( array(
    'id'        => $prefix . 'about_hero_image',
    'name'      => esc_html__('Imagen Flotante del Hero', 'tisserie'),
    'desc'      => esc_html__('Cargar una imagen que flotará bajo este Hero', 'tisserie'),
    'type'      => 'file',

    'options'   => array(
        'url'   => false
    ),
    'text'      => array(
        'add_upload_file_text' => esc_html__('Cargar fondo', 'tisserie'),
    ),
    'query_args' => array(
        'type'   => array(
            'image/gif',
            'image/jpeg',
            'image/png'
        )
    ),
    'preview_size' => 'thumbnail'
));

/* --------------------------------------------------------------
    2.- ABOUT: HISTORY SECTION
-------------------------------------------------------------- */

$cmb_about_history = new_cmb2_box(array(
    'id'            => $prefix . 'about_history_metabox',
    'title'         => esc_html__('About: Sección Historia', 'tisserie'),
    'object_types'  => array('page'),
    'show_on'       => array('key' => 'page-template', 'value' => 'templates/page-about.php'),
    'context'       => 'normal',
    'priority'      => 'high',
    'show_names'    => true,
    'cmb_styles'    => true,
    'closed'        => false
));

$cmb_about_history->add_field( array(
    'id'        => $prefix . 'about_history_bg',
    'name'      => esc_html__('Imagen de Fondo de la Sección', 'tisserie'),
    'desc'      => esc_html__('Cargar un fondo para esta Sección', 'tisserie'),
    'type'      => 'file',

    'options'   => array(
        'url'   => false
    ),
    'text'      => array(
        'add_upload_file_text' => esc_html__('Cargar fondo', 'tisserie'),
    ),
    'query_args' => array(
        'type'   => array(
            'image/gif',
            'image/jpeg',
            'image/png'
        )
    ),
    'preview_size' => 'thumbnail'
));

$cmb_about_history->add_field( array(
    'id'        => $prefix . 'about_history_title',
    'name'      => esc_html__('Título de la Sección', 'tisserie'),
    'desc'      => esc_html__('Ingrese el Título de la Sección', 'tisserie'),
    'type'      => 'text'
));


$cmb_about_history->add_field( array(
    'id'        => $prefix . 'about_history_desc',
    'name'      => esc_html__('Descripción de la Sección', 'tisserie'),
    'desc'      => esc_html__('Ingrese la descripción de la Sección', 'tisserie'),
    'type'      => 'wysiwyg',
    'options'   => array(
        'textarea_rows' => get_option('default_post_edit_rows', 2),
        'teeny' => false
    )
));

$cmb_about_history->add_field( array(
    'id'        => $prefix . 'about_history_image',
    'name'      => esc_html__('Imagen Flotante de la Sección', 'tisserie'),
    'desc'      => esc_html__('Cargar una imagen que flotará en la derecha de la Sección', 'tisserie'),
    'type'      => 'file',

    'options'   => array(
        'url'   => false
    ),
    'text'      => array(
        'add_upload_file_text' => esc_html__('Cargar Imagen', 'tisserie'),
    ),
    'query_args' => array(
        'type'   => array(
            'image/gif',
            'image/jpeg',
            'image/png'
        )
    ),
    'preview_size' => 'thumbnail'
));

/* --------------------------------------------------------------
    3.- ABOUT: DESCANSO SECTION
-------------------------------------------------------------- */

$cmb_about_today = new_cmb2_box(array(
    'id'            => $prefix . 'about_today_metabox',
    'title'         => esc_html__('About: Sección Historia', 'tisserie'),
    'object_types'  => array('page'),
    'show_on'       => array('key' => 'page-template', 'value' => 'templates/page-about.php'),
    'context'       => 'normal',
    'priority'      => 'high',
    'show_names'    => true,
    'cmb_styles'    => true,
    'closed'        => false
));

$cmb_about_today->add_field( array(
    'id'        => $prefix . 'about_today_desc1',
    'name'      => esc_html__('Descripción 1 de la Sección', 'tisserie'),
    'desc'      => esc_html__('Ingrese la descripción de la Sección antes del texto grande', 'tisserie'),
    'type'      => 'wysiwyg',
    'options'   => array(
        'textarea_rows' => get_option('default_post_edit_rows', 2),
        'teeny' => false
    )
));

$cmb_about_today->add_field( array(
    'id'        => $prefix . 'about_today_title',
    'name'      => esc_html__('Título de la Sección', 'tisserie'),
    'desc'      => esc_html__('Ingrese el Título de la Sección', 'tisserie'),
    'type'      => 'text'
));

$cmb_about_today->add_field( array(
    'id'        => $prefix . 'about_today_desc2',
    'name'      => esc_html__('Descripción 2 de la Sección', 'tisserie'),
    'desc'      => esc_html__('Ingrese la descripción de la Sección despues del texto grande', 'tisserie'),
    'type'      => 'wysiwyg',
    'options'   => array(
        'textarea_rows' => get_option('default_post_edit_rows', 2),
        'teeny' => false
    )
));

$cmb_about_today->add_field( array(
    'id'        => $prefix . 'about_today_image',
    'name'      => esc_html__('Imagen Flotante de la Sección', 'tisserie'),
    'desc'      => esc_html__('Cargar una imagen que flotará en la izquierda de la Sección', 'tisserie'),
    'type'      => 'file',

    'options'   => array(
        'url'   => false
    ),
    'text'      => array(
        'add_upload_file_text' => esc_html__('Cargar Imagen', 'tisserie'),
    ),
    'query_args' => array(
        'type'   => array(
            'image/gif',
            'image/jpeg',
            'image/png'
        )
    ),
    'preview_size' => 'thumbnail'
));


/* --------------------------------------------------------------
    3.- ABOUT: BANNER SECTION
-------------------------------------------------------------- */

$cmb_about_banner = new_cmb2_box(array(
    'id'            => $prefix . 'about_banner_metabox',
    'title'         => esc_html__('About: Sección Banner con Texto', 'tisserie'),
    'object_types'  => array('page'),
    'show_on'       => array('key' => 'page-template', 'value' => 'templates/page-about.php'),
    'context'       => 'normal',
    'priority'      => 'high',
    'show_names'    => true,
    'cmb_styles'    => true,
    'closed'        => false
));

$group_field_id = $cmb_about_banner->add_field( array(
    'id'          => $prefix . 'about_banner_group',
    'name'      => esc_html__( 'Grupos de Slides', 'tisserie' ),
    'description' => __( 'Slides dentro del Hero', 'tisserie' ),
    'type'        => 'group',
    'options'     => array(
        'group_title'       => __( 'Slide {#}', 'tisserie' ),
        'add_button'        => __( 'Agregar otro Slide', 'tisserie' ),
        'remove_button'     => __( 'Remover Slide', 'tisserie' ),
        'sortable'          => true,
        'closed'         => true,
        'remove_confirm' => esc_html__( '¿Estas seguro de remover este Slide?', 'tisserie' )
    )
) );

$cmb_about_banner->add_group_field( $group_field_id, array(
    'id'        => 'desc',
    'name'      => esc_html__('Cita del Banner', 'tisserie'),
    'desc'      => esc_html__('Ingrese la Cita del Banner', 'tisserie'),
    'type'      => 'wysiwyg',
    'options'   => array(
        'textarea_rows' => get_option('default_post_edit_rows', 2),
        'teeny' => false
    )
));

$cmb_about_banner->add_group_field( $group_field_id, array(
    'id'        => 'author',
    'name'      => esc_html__('Autor de la Cita', 'tisserie'),
    'desc'      => esc_html__('Ingrese el Autor de la Cita', 'tisserie'),
    'type'      => 'text'
));

/* --------------------------------------------------------------
    4.- ABOUT: GALERÍA SECTION
-------------------------------------------------------------- */

$cmb_about_footer_gallery_section = new_cmb2_box(array(
    'id'            => $prefix . 'about_footer_gallery_metabox',
    'title'         => esc_html__('About: Galería', 'tisserie'),
    'object_types'  => array('page'),
    'show_on'      => array('key' => 'page-template', 'value' => 'templates/page-about.php'),
    'context'    => 'normal',
    'priority'   => 'high',
    'show_names' => true,
    'cmb_styles' => true,
    'closed'     => false
));

$cmb_about_footer_gallery_section->add_field(array(
    'id'            => $prefix . 'about_footer_gallery_list',
    'name'          => esc_html__('Galería', 'tisserie'),
    'desc'          => esc_html__('Cargue o seleccione las imágenes para incluirlas en la Galería', 'tisserie'),
    'type'          => 'file_list',
    'query_args'    => array('type' => 'image'),
    'text' => array(
        'add_upload_files_text'     => esc_html__('Cargar Imágenes', 'tisserie'),
        'remove_image_text'         => esc_html__('Remove Imágenes', 'tisserie'),
        'file_text'                 => esc_html__('Imagen', 'tisserie'),
        'file_download_text'        => esc_html__('Descargar', 'tisserie'),
        'remove_text'               => esc_html__('Remover', 'tisserie'),
    ),
    'preview_size'  => 'thumbnail'
));
