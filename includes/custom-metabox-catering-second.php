<?php

/* --------------------------------------------------------------
    1.- CATERING - OPCIONES PRINCIPALES
-------------------------------------------------------------- */
$cmb_catering2_main_options = new_cmb2_box(array(
    'id'            => $prefix . 'catering_main_options_metabox',
    'title'         => esc_html__('Catering: Opciones Principales', 'tisserie'),
    'object_types'  => array('page'),
    'show_on'       => array('key' => 'page-template', 'value' => 'templates/page-catering-second.php'),
    'context'       => 'normal',
    'priority'      => 'high',
    'show_names'    => true,
    'cmb_styles'    => true,
    'closed'        => false
));

$group_field_id = $cmb_catering2_main_options->add_field( array(
    'id'          => $prefix . 'catering_main_options_group',
    'name'      => esc_html__( 'Grupos de Opciones', 'tisserie' ),
    'description' => __( 'Slides dentro del Opciones', 'tisserie' ),
    'type'        => 'group',
    'options'     => array(
        'group_title'       => __( 'Opcion {#}', 'tisserie' ),
        'add_button'        => __( 'Agregar otra Opcion', 'tisserie' ),
        'remove_button'     => __( 'Remover Opcion', 'tisserie' ),
        'sortable'          => true,
        'closed'         => true,
        'remove_confirm' => esc_html__( '¿Estas seguro de remover esta Opcion?', 'tisserie' )
    )
) );

$cmb_catering2_main_options->add_group_field( $group_field_id, array(
    'id'        => 'desc',
    'name'      => esc_html__( 'Descripción', 'tisserie' ),
    'desc'      => esc_html__( 'Ingrese la descripción de la Opcion', 'tisserie' ),
    'type' => 'wysiwyg',
    'options' => array(
        'textarea_rows' => get_option('default_post_edit_rows', 2),
        'teeny' => false
    )
) );

$cmb_catering2_main_options->add_group_field( $group_field_id, array(
    'id'        => 'price',
    'name'      => esc_html__( 'Precio', 'tisserie' ),
    'desc'      => esc_html__( 'Ingrese el precio de la Opcion', 'tisserie' ),
    'type' => 'text'
) );

$cmb_catering2_main_options->add_group_field( $group_field_id, array(
    'id'        => 'additional',
    'name'      => esc_html__( 'Información Adicional', 'tisserie' ),
    'desc'      => esc_html__( 'Ingrese cualquier Información Adicional que corresponda', 'tisserie' ),
    'type' => 'text'
) );

/* --------------------------------------------------------------
   2.- CATERING - OPCIONES GENERALES
-------------------------------------------------------------- */
$cmb_catering2_general_options = new_cmb2_box(array(
    'id'            => $prefix . 'catering_general_options_metabox',
    'title'         => esc_html__('Catering: Opciones Generales', 'tisserie'),
    'object_types'  => array('page'),
    'show_on'       => array('key' => 'page-template', 'value' => 'templates/page-catering-second.php'),
    'context'       => 'normal',
    'priority'      => 'high',
    'show_names'    => true,
    'cmb_styles'    => true,
    'closed'        => false
));

$group_field_id = $cmb_catering2_general_options->add_field( array(
    'id'          => $prefix . 'catering_general_options_group',
    'name'      => esc_html__( 'Grupos de Opciones', 'tisserie' ),
    'description' => __( 'Slides dentro del Opciones', 'tisserie' ),
    'type'        => 'group',
    'options'     => array(
        'group_title'       => __( 'Opcion {#}', 'tisserie' ),
        'add_button'        => __( 'Agregar otra Opcion', 'tisserie' ),
        'remove_button'     => __( 'Remover Opcion', 'tisserie' ),
        'sortable'          => true,
        'closed'         => true,
        'remove_confirm' => esc_html__( '¿Estas seguro de remover esta Opcion?', 'tisserie' )
    )
) );

$cmb_catering2_general_options->add_group_field( $group_field_id, array(
    'id'        => 'repeat',
    'name'      => esc_html__( 'Esta opcion es una repetición del anterior?', 'tisserie' ),
    'desc'      => esc_html__( 'Marque esta opcion si la opción anterior es igual a esta (Solo se cambiaran precios)', 'tisserie' ),
    'type' => 'checkbox'
) );


$cmb_catering2_general_options->add_group_field( $group_field_id, array(
    'id'        => 'desc',
    'name'      => esc_html__( 'Descripción', 'tisserie' ),
    'desc'      => esc_html__( 'Ingrese la descripción de la Opcion', 'tisserie' ),
    'type' => 'wysiwyg',
    'options' => array(
        'textarea_rows' => get_option('default_post_edit_rows', 2),
        'teeny' => false
    )
) );

$cmb_catering2_general_options->add_group_field( $group_field_id, array(
    'id'        => 'price',
    'name'      => esc_html__( 'Precio', 'tisserie' ),
    'desc'      => esc_html__( 'Ingrese el precio de la Opcion', 'tisserie' ),
    'type' => 'text'
) );

$cmb_catering2_general_options->add_group_field( $group_field_id, array(
    'id'        => 'additional',
    'name'      => esc_html__( 'Información Adicional', 'tisserie' ),
    'desc'      => esc_html__( 'Ingrese cualquier Información Adicional que corresponda', 'tisserie' ),
    'type' => 'text'
) );

/* --------------------------------------------------------------
    3.- CATERING - CONTENIDO ADICIONAL
-------------------------------------------------------------- */
$cmb_catering2_general_content = new_cmb2_box(array(
    'id'            => $prefix . 'catering_general_content_metabox',
    'title'         => esc_html__('Catering: Contenido General', 'tisserie'),
    'object_types'  => array('page'),
    'show_on'       => array('key' => 'page-template', 'value' => 'templates/page-catering-second.php'),
    'context'       => 'normal',
    'priority'      => 'high',
    'show_names'    => true,
    'cmb_styles'    => true,
    'closed'        => false
));

$cmb_catering2_general_content->add_field( array(
    'id'   => $prefix . 'catering_boxed_content',
    'name'      => esc_html__( 'Contenido dentro del marco', 'tisserie' ),
    'desc'      => esc_html__( 'Ingrese el texto contenido dentro del marco', 'tisserie' ),
    'type' => 'wysiwyg',
    'options' => array(
        'textarea_rows' => get_option('default_post_edit_rows', 2),
        'teeny' => false
    )
) );

$cmb_catering2_general_content->add_field( array(
    'id'   => $prefix . 'catering_main_content',
    'name'      => esc_html__( 'Contenido principal', 'tisserie' ),
    'desc'      => esc_html__( 'Ingrese el texto descriptivo', 'tisserie' ),
    'type' => 'wysiwyg',
    'options' => array(
        'textarea_rows' => get_option('default_post_edit_rows', 2),
        'teeny' => false
    )
) );

$cmb_catering2_general_content->add_field( array(
    'id'   => $prefix . 'catering_phone_text',
    'name'      => esc_html__( 'Telefono de la Sección', 'tisserie' ),
    'desc'      => esc_html__( 'Ingrese el número telefonico de la sección', 'tisserie' ),
    'type' => 'text'
) );

$cmb_catering2_general_content->add_field( array(
    'id'   => $prefix . 'catering_email_text',
    'name'      => esc_html__( 'Correo de la Sección', 'tisserie' ),
    'desc'      => esc_html__( 'Ingrese el Correo Electrónico de la sección', 'tisserie' ),
    'type' => 'text'
) );