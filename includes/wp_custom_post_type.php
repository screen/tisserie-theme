<?php
/*
function tisserie_custom_post_type() {

	$labels = array(
		'name'                  => _x( 'Clientes', 'Post Type General Name', 'tisserie' ),
		'singular_name'         => _x( 'Cliente', 'Post Type Singular Name', 'tisserie' ),
		'menu_name'             => __( 'Clientes', 'tisserie' ),
		'name_admin_bar'        => __( 'Clientes', 'tisserie' ),
		'archives'              => __( 'Archivo de Clientes', 'tisserie' ),
		'attributes'            => __( 'Atributos de Cliente', 'tisserie' ),
		'parent_item_colon'     => __( 'Cliente Padre:', 'tisserie' ),
		'all_items'             => __( 'Todos los Clientes', 'tisserie' ),
		'add_new_item'          => __( 'Agregar Nuevo Cliente', 'tisserie' ),
		'add_new'               => __( 'Agregar Nuevo', 'tisserie' ),
		'new_item'              => __( 'Nuevo Cliente', 'tisserie' ),
		'edit_item'             => __( 'Editar Cliente', 'tisserie' ),
		'update_item'           => __( 'Actualizar Cliente', 'tisserie' ),
		'view_item'             => __( 'Ver Cliente', 'tisserie' ),
		'view_items'            => __( 'Ver Clientes', 'tisserie' ),
		'search_items'          => __( 'Buscar Cliente', 'tisserie' ),
		'not_found'             => __( 'No hay resultados', 'tisserie' ),
		'not_found_in_trash'    => __( 'No hay resultados en Papelera', 'tisserie' ),
		'featured_image'        => __( 'Imagen del Cliente', 'tisserie' ),
		'set_featured_image'    => __( 'Colocar Imagen del Cliente', 'tisserie' ),
		'remove_featured_image' => __( 'Remover Imagen del Cliente', 'tisserie' ),
		'use_featured_image'    => __( 'Usar como Imagen del Cliente', 'tisserie' ),
		'insert_into_item'      => __( 'Insertar en Cliente', 'tisserie' ),
		'uploaded_to_this_item' => __( 'Cargado a este Cliente', 'tisserie' ),
		'items_list'            => __( 'Listado de Clientes', 'tisserie' ),
		'items_list_navigation' => __( 'Navegación del Listado de Cliente', 'tisserie' ),
		'filter_items_list'     => __( 'Filtro del Listado de Clientes', 'tisserie' ),
	);
	$args = array(
		'label'                 => __( 'Cliente', 'tisserie' ),
		'description'           => __( 'Portafolio de Clientes', 'tisserie' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail' ),
		'taxonomies'            => array( 'tipos-de-clientes' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 15,
		'menu_icon'             => 'dashicons-businessperson',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
		'show_in_rest'          => true,
	);
	register_post_type( 'clientes', $args );

}

add_action( 'init', 'tisserie_custom_post_type', 0 );
*/
