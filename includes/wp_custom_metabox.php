<?php
function ed_metabox_include_front_page( $display, $meta_box ) {
    if ( ! isset( $meta_box['show_on']['key'] ) ) {
        return $display;
    }

    if ( 'front-page' !== $meta_box['show_on']['key'] ) {
        return $display;
    }

    $post_id = 0;

    // If we're showing it based on ID, get the current ID
    if ( isset( $_GET['post'] ) ) {
        $post_id = $_GET['post'];
    } elseif ( isset( $_POST['post_ID'] ) ) {
        $post_id = $_POST['post_ID'];
    }

    if ( ! $post_id ) {
        return false;
    }

    // Get ID of page set as front page, 0 if there isn't one
    $front_page = get_option( 'page_on_front' );

    // there is a front page set and we're on it!
    return $post_id == $front_page;
}
add_filter( 'cmb2_show_on', 'ed_metabox_include_front_page', 10, 2 );

function be_metabox_show_on_slug( $display, $meta_box ) {
    if ( ! isset( $meta_box['show_on']['key'], $meta_box['show_on']['value'] ) ) {
        return $display;
    }

    if ( 'slug' !== $meta_box['show_on']['key'] ) {
        return $display;
    }

    $post_id = 0;

    // If we're showing it based on ID, get the current ID
    if ( isset( $_GET['post'] ) ) {
        $post_id = $_GET['post'];
    } elseif ( isset( $_POST['post_ID'] ) ) {
        $post_id = $_POST['post_ID'];
    }

    if ( ! $post_id ) {
        return $display;
    }

    $slug = get_post( $post_id )->post_name;

    // See if there's a match
    return in_array( $slug, (array) $meta_box['show_on']['value']);
}
add_filter( 'cmb2_show_on', 'be_metabox_show_on_slug', 10, 2 );

add_action( 'cmb2_admin_init', 'tisserie_register_custom_metabox' );
function tisserie_register_custom_metabox() {
    $prefix = 'tsr_';

    $cmb_metabox = new_cmb2_box( array(
        'id'            => $prefix . 'page_metabox',
        'title'         => esc_html__( 'Opciones en Menu', 'tisserie' ),
        'object_types'  => array( 'page' ), // Post type
        'context'       => 'side'
    ) );

    $cmb_metabox->add_field( array(
        'name'       => esc_html__( 'Activar Menu para Fondo Claro', 'tisserie' ),
        'desc'       => esc_html__( 'Active este checkbox si el fondo de esta página es blanco o de colores claros', 'tisserie' ),
        'id'         => $prefix . 'header_black',
        'type'       => 'checkbox'
    ) );

    /* LANDING HOME */
    require_once('custom-metabox-landing.php');

    /* LANDING THANKS */
    require_once('custom-metabox-thanks.php');

    /* HOME METABOXES */
    require_once('custom-metabox-home.php');

    /* ABOUT METABOXES */
    require_once('custom-metabox-about.php');

    /* STORE METABOXES */
    require_once('custom-metabox-store.php');

    /* CATERING METABOXES */
    require_once('custom-metabox-catering.php');    

    /* CATERING SECOND METABOXES */
    require_once('custom-metabox-catering-second.php');    

    /* WHOLESALE METABOXES */
    require_once('custom-metabox-wholesale.php');    
    
    /* DELIVERY METABOXES */
    require_once('custom-metabox-delivery.php');

    /* CONTACT METABOXES */
    require_once('custom-metabox-contact.php');
}