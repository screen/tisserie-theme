<?php
/* --------------------------------------------------------------
WP CUSTOMIZE SECTION - CUSTOM SETTINGS
-------------------------------------------------------------- */

add_action( 'customize_register', 'tisserie_customize_register' );

function tisserie_customize_register( $wp_customize ) {

    /* SOCIAL SETTINGS */
    $wp_customize->add_section('tsr_social_settings', array(
        'title'    => __('Redes Sociales', 'tisserie'),
        'description' => __('Agregue aqui las redes sociales de la página, serán usadas globalmente', 'tisserie'),
        'priority' => 175,
    ));

    $wp_customize->add_setting('tsr_social_settings[facebook]', array(
        'default'           => '',
        'sanitize_callback' => 'tisserie_sanitize_url',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',
    ));

    $wp_customize->add_control( 'facebook', array(
        'type' => 'url',
        'section' => 'tsr_social_settings',
        'settings' => 'tsr_social_settings[facebook]',
        'label' => __( 'Facebook', 'tisserie' ),
    ));

    $wp_customize->add_setting('tsr_social_settings[twitter]', array(
        'default'           => '',
        'sanitize_callback' => 'tisserie_sanitize_url',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',
    ));

    $wp_customize->add_control( 'twitter', array(
        'type' => 'url',
        'section' => 'tsr_social_settings',
        'settings' => 'tsr_social_settings[twitter]',
        'label' => __( 'Twitter', 'tisserie' ),
    ));

    $wp_customize->add_setting('tsr_social_settings[instagram]', array(
        'default'           => '',
        'sanitize_callback' => 'tisserie_sanitize_url',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',

    ));

    $wp_customize->add_control( 'instagram', array(
        'type' => 'url',
        'section' => 'tsr_social_settings',
        'settings' => 'tsr_social_settings[instagram]',
        'label' => __( 'Instagram', 'tisserie' ),
    ));

    $wp_customize->add_setting('tsr_social_settings[linkedin]', array(
        'default'           => '',
        'sanitize_callback' => 'tisserie_sanitize_url',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',
    ));

    $wp_customize->add_control( 'linkedin', array(
        'type' => 'url',
        'section' => 'tsr_social_settings',
        'settings' => 'tsr_social_settings[linkedin]',
        'label' => __( 'LinkedIn', 'tisserie' ),
    ));

    $wp_customize->add_setting('tsr_social_settings[youtube]', array(
        'default'           => '',
        'sanitize_callback' => 'tisserie_sanitize_url',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',

    ));

    $wp_customize->add_control( 'youtube', array(
        'type' => 'url',
        'section' => 'tsr_social_settings',
        'settings' => 'tsr_social_settings[youtube]',
        'label' => __( 'YouTube', 'tisserie' ),
    ) );

    /* MODAL */
    $wp_customize->add_section('tsr_modal_settings', array(
        'title'    => __('Modal', 'pinyacampoy'),
        'description' => __('Opciones de Modal', 'pinyacampoy'),
        'priority' => 176,
    ));

    $wp_customize->add_setting('tsr_modal_settings[modal_title]', array(
        'default'           => '',
        'sanitize_callback' => 'sanitize_text_field',
        'capability'        => 'edit_theme_options',
        'type'           => 'option'

    ));

    $wp_customize->add_control( 'modal_title', array(
        'type' => 'text',
        'label'    => __('Título', 'pinyacampoy'),
        'description' => __( 'Título descriptivo del Modal.' ),
        'section'  => 'tsr_modal_settings',
        'settings' => 'tsr_modal_settings[modal_title]'
    ));

    $wp_customize->add_setting('tsr_modal_settings[modal_text]', array(
        'default'           => '',
        'sanitize_callback' => 'sanitize_text_field',
        'capability'        => 'edit_theme_options',
        'type'           => 'option'

    ));

    $wp_customize->add_control( 'modal_text', array(
        'type' => 'textarea',
        'label'    => __('Descripción', 'pinyacampoy'),
        'description' => __( 'Texto descriptivo del Modal.' ),
        'section'  => 'tsr_modal_settings',
        'settings' => 'tsr_modal_settings[modal_text]'
    ));

    $wp_customize->add_setting('tsr_modal_settings[modal_link]', array(
        'default'           => '',
        'sanitize_callback' => 'absint',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',
    ));

    $wp_customize->add_control( 'modal_link', array(
        'type'     => 'dropdown-pages',
        'label' => __( 'Link Pagina de Agradecimiento', 'pinyacampoy' ),
        'section' => 'tsr_modal_settings',
        'settings' => 'tsr_modal_settings[modal_link]',
    ) );

    /* COOKIES SETTINGS */
    $wp_customize->add_section('tsr_cookie_settings', array(
        'title'    => __('Cookies', 'tisserie'),
        'description' => __('Opciones de Cookies', 'tisserie'),
        'priority' => 176,
    ));

    $wp_customize->add_setting('tsr_cookie_settings[cookie_text]', array(
        'default'           => '',
        'sanitize_callback' => 'sanitize_text_field',
        'capability'        => 'edit_theme_options',
        'type'           => 'option'

    ));

    $wp_customize->add_control( 'cookie_text', array(
        'type' => 'textarea',
        'label'    => __('Cookie consent', 'tisserie'),
        'description' => __( 'Texto del Cookie consent.' ),
        'section'  => 'tsr_cookie_settings',
        'settings' => 'tsr_cookie_settings[cookie_text]'
    ));

    $wp_customize->add_setting('tsr_cookie_settings[cookie_link]', array(
        'default'           => '',
        'sanitize_callback' => 'absint',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',

    ));

    $wp_customize->add_control( 'cookie_link', array(
        'type'     => 'dropdown-pages',
        'section' => 'tsr_cookie_settings',
        'settings' => 'tsr_cookie_settings[cookie_link]',
        'label' => __( 'Link de Cookies', 'tisserie' ),
    ) );

}

function tisserie_sanitize_url( $url ) {
    return esc_url_raw( $url );
}

/* --------------------------------------------------------------
CUSTOM CONTROL PANEL
-------------------------------------------------------------- */
/*
function register_tisserie_settings() {
    register_setting( 'tisserie-settings-group', 'monday_start' );
    register_setting( 'tisserie-settings-group', 'monday_end' );
    register_setting( 'tisserie-settings-group', 'monday_all' );
}

add_action('admin_menu', 'tisserie_custom_panel_control');

function tisserie_custom_panel_control() {
    add_menu_page(
        __( 'Panel de Control', 'tisserie' ),
        __( 'Panel de Control','tisserie' ),
        'manage_options',
        'tisserie-control-panel',
        'tisserie_control_panel_callback',
        'dashicons-admin-generic',
        120
    );
    add_action( 'admin_init', 'register_tisserie_settings' );
}

function tisserie_control_panel_callback() {
    ob_start();
?>
<div class="tisserie-admin-header-container">
    <img src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt="tisserie" />
    <h1><?php echo esc_html( get_admin_page_title() ); ?></h1>
</div>
<form method="post" action="options.php" class="tisserie-admin-content-container">
    <?php settings_fields( 'tisserie-settings-group' ); ?>
    <?php do_settings_sections( 'tisserie-settings-group' ); ?>
    <div class="tisserie-admin-content-item">
        <table class="form-table">
            <tr valign="center">
                <th scope="row"><?php _e('Monday', 'tisserie'); ?></th>
                <td>
                    <label for="monday_start">Starting Hour: <input type="time" name="monday_start" value="<?php echo esc_attr( get_option('monday_start') ); ?>"></label>
                    <label for="monday_end">Ending Hour: <input type="time" name="monday_end" value="<?php echo esc_attr( get_option('monday_end') ); ?>"></label>
                    <label for="monday_all">All Day: <input type="checkbox" name="monday_all" value="1" <?php checked( get_option('monday_all'), 1 ); ?>></label>
                </td>
            </tr>
        </table>
    </div>
    <div class="tisserie-admin-content-submit">
        <?php submit_button(); ?>
    </div>
</form>
<?php
    $content = ob_get_clean();
    echo $content;
}
*/
