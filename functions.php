<?php

/* --------------------------------------------------------------
    ENQUEUE AND REGISTER CSS
-------------------------------------------------------------- */

require_once('includes/wp_enqueue_styles.php');

/* --------------------------------------------------------------
    ENQUEUE AND REGISTER JS
-------------------------------------------------------------- */

if (!is_admin()) add_action('wp_enqueue_scripts', 'tisserie_jquery_enqueue');
function tisserie_jquery_enqueue() {
    wp_deregister_script('jquery');
    wp_deregister_script('jquery-migrate');
    if ($_SERVER['REMOTE_ADDR'] == '::1') {
        /*- JQUERY ON LOCAL  -*/
        wp_register_script( 'jquery', get_template_directory_uri() . '/js/jquery.min.js', false, '3.5.1', false);
        /*- JQUERY MIGRATE ON LOCAL  -*/
        wp_register_script( 'jquery-migrate', get_template_directory_uri() . '/js/jquery-migrate.min.js',  array('jquery'), '3.1.0', false);
    } else {
        /*- JQUERY ON WEB  -*/
        wp_register_script( 'jquery', 'https://code.jquery.com/jquery-3.5.1.min.js', false, '3.5.1', false);
        /*- JQUERY MIGRATE ON WEB  -*/
        wp_register_script( 'jquery-migrate', 'https://code.jquery.com/jquery-migrate-3.3.1.js', array('jquery'), '3.3.1', true);
    }
    wp_enqueue_script('jquery');
    wp_enqueue_script('jquery-migrate');
}

/* NOW ALL THE JS FILES */

require_once('includes/wp_enqueue_scripts.php');

/* --------------------------------------------------------------
    ADD CUSTOM WALKER BOOTSTRAP
-------------------------------------------------------------- */

add_action( 'after_setup_theme', 'tisserie_register_navwalker' );
function tisserie_register_navwalker(){
    require_once('includes/class-wp-bootstrap-navwalker.php');
}

/* --------------------------------------------------------------
    ADD CUSTOM WORDPRESS FUNCTIONS
-------------------------------------------------------------- */

require_once('includes/wp_custom_functions.php');

/* --------------------------------------------------------------
    ADD REQUIRED WORDPRESS PLUGINS
-------------------------------------------------------------- */

require_once('includes/class-tgm-plugin-activation.php');
require_once('includes/class-required-plugins.php');

/* --------------------------------------------------------------
    ADD CUSTOM WOOCOMMERCE OVERRIDES
-------------------------------------------------------------- */
if ( class_exists( 'WooCommerce' ) ) {
    require_once('includes/wp_woocommerce_functions.php');
}

/* --------------------------------------------------------------
    ADD JETPACK COMPATIBILITY
-------------------------------------------------------------- */
if ( defined( 'JETPACK__VERSION' ) ) {
    require_once('includes/wp_jetpack_functions.php');
}

/* --------------------------------------------------------------
    ADD NAV MENUS LOCATIONS
-------------------------------------------------------------- */

register_nav_menus( array(
    'header_menu' => __( 'Menu Header - Principal', 'tisserie' )
) );

/* --------------------------------------------------------------
    ADD DYNAMIC SIDEBAR SUPPORT
-------------------------------------------------------------- */

add_action( 'widgets_init', 'tisserie_widgets_init' );

function tisserie_widgets_init() {
    register_sidebar( array(
        'name' => __( 'Sidebar Principal', 'tisserie' ),
        'id' => 'main_sidebar',
        'description' => __( 'Estos widgets seran vistos en las entradas y páginas del sitio', 'tisserie' ),
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
        'after_widget'  => '</li>',
        'before_title'  => '<h2 class="widgettitle">',
        'after_title'   => '</h2>',
    ) );

    register_sidebars( 4, array(
        'name'          => __('Pie de Página %d', 'tisserie'),
        'id'            => 'sidebar_footer',
        'description'   => __('Estos widgets seran vistos en el pie de página del sitio', 'tisserie'),
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
        'after_widget'  => '</li>',
        'before_title'  => '<h2 class="widgettitle">',
        'after_title'   => '</h2>'
    ) );

}

/* --------------------------------------------------------------
    ADD CUSTOM METABOX
-------------------------------------------------------------- */

require_once('includes/wp_custom_metabox.php');

/* --------------------------------------------------------------
    ADD CUSTOM POST TYPE
-------------------------------------------------------------- */

//require_once('includes/wp_custom_post_type.php');

/* --------------------------------------------------------------
    ADD CUSTOM THEME CONTROLS
-------------------------------------------------------------- */

require_once('includes/wp_custom_theme_control.php');

/* --------------------------------------------------------------
    ADD CUSTOM IMAGE SIZE
-------------------------------------------------------------- */
if ( function_exists('add_theme_support') ) {
    add_theme_support('post-thumbnails');
    set_post_thumbnail_size( 9999, 400, true);
}
if ( function_exists('add_image_size') ) {
    add_image_size('avatar', 100, 100, true);
    add_image_size('blog_img', 276, 217, true);
    add_image_size('single_img', 636, 297, true );
    add_image_size('catalog_img', 300, 300, true );
    add_image_size('boxed_img', 500, 500, true );
    add_image_size('special_large', 630, 895, true );
    add_image_size('special_normal', 630, 420, true );
    add_image_size('special_medium', 630, 500, true );
}

/* --------------------------------------------------------------
    ADD CUSTOM AJAX HANDLER
-------------------------------------------------------------- */
add_action('wp_ajax_nopriv_send_message', 'send_message_handler');
add_action('wp_ajax_send_message', 'send_message_handler');

function send_message_handler() {
    if ( defined( 'WP_DEBUG' ) && WP_DEBUG ) {
        error_reporting( E_ALL );
        ini_set( 'display_errors', 1 );
    }

    $full_name = $_POST['modal-name'];
    $email = $_POST['modal-email'];
    $phone = $_POST['modal-phone'];
    $address = $_POST['modal-address'];
    $zipcode = $_POST['modal-zipcode'];
    $role = $_POST['modal-role'];
    $company_name = $_POST['modal-company-name'];
    $company_size = $_POST['modal-company-size'];
    $business_type = $_POST['modal-business-type'];
    $frequency = $_POST['modal-frequency'];

    $logo = get_template_directory_uri() . '/images/logo.png';
    //    $grecaptcha = $_POST['g-recaptcha-response'];

    //    $google_options = get_option('hlp_google_settings');

    //    if ($grecaptcha) {
    //        $post_data = http_build_query(
    //            array(
    //                'secret' => $google_options['secretkey'],
    //                'response' => $grecaptcha,
    //                'remoteip' => $_SERVER['REMOTE_ADDR']
    //            ), '', '&');
    //
    //        $opts = array('http' =>
    //                      array(
    //                          'method'  => 'POST',
    //                          'header'  => 'Content-type: application/x-www-form-urlencoded',
    //                          'content' => $post_data
    //                      )
    //                     );
    //
    //        $context  = stream_context_create($opts);
    //        $response = file_get_contents('https://www.google.com/recaptcha/api/siteverify', false, $context);
    //        $captcha_response = json_decode($response);
    //    }
    //
    //    if($captcha_response->success == true) {
    ob_start();
    require_once get_theme_file_path( '/templates/contact-email.php' );
    $body = ob_get_clean();
    $body = str_replace( [
        '{fullname}',
        '{email}',
        '{phone}',
        '{address}',
        '{zipcode}',
        '{role}',
        '{company_name}',
        '{company_size}',
        '{business_type}',
        '{frequency}',
        '{logo}'
    ], [
        $full_name,
        $email,
        $phone,
        $address,
        $zipcode,
        $role,
        $company_name,
        $company_size,
        $business_type,
        $frequency,
        $logo
    ], $body );

    $path = ABSPATH . WPINC . '/class-phpmailer.php';

    if (file_exists($path)) {
        require_once($path);
        $mail = new PHPMailer;
    } else {
        require_once ABSPATH . WPINC . '/PHPMailer/PHPMailer.php';
        $mail = new PHPMailer\PHPMailer\PHPMailer;
    }

    $mail->isHTML( true );
    $mail->Body = $body;
    $mail->CharSet = 'UTF-8';
    $mail->addAddress( 'ochoa.robert1@gmail.com' );
    //    $mail->addAddress( 'nefrain@gmail.com' );
    $mail->setFrom( "noreply@{$_SERVER['SERVER_NAME']}", esc_html( get_bloginfo( 'name' ) ) );
    $mail->Subject = esc_html__( 'Tisserie: New Message Request', 'tisserie' );

    if ( ! $mail->send() ) {
        wp_send_json_error( esc_html__( 'Your request could not be sent. Please try again.', 'tisserie' ), 400 );
    } else {
        wp_send_json_success( esc_html__( "Thank You for your interest in our products, you will be contacted shortly.", 'tisserie' ), 200 );
    }

    //    } else {
    //        wp_send_json_error( esc_html__( 'Your request2 could not be sent. Please try again.', 'holpack' ), 400 );
    //    }

    wp_die();
}

/* --------------------------------------------------------------
    ADD CUSTOM AJAX HANDLER
-------------------------------------------------------------- */
add_action('wp_ajax_nopriv_contact_message', 'contact_message_handler');
add_action('wp_ajax_contact_message', 'contact_message_handler');

function contact_message_handler() {
    if ( defined( 'WP_DEBUG' ) && WP_DEBUG ) {
        error_reporting( E_ALL );
        ini_set( 'display_errors', 1 );
    }

    $full_name = $_POST['contact-name'];
    $email = $_POST['contact-email'];
    $phone = $_POST['contact-phone'];
    $company_name = $_POST['contact-company-name'];
    $subject = $_POST['contact-subject'];
    $message = $_POST['contact-message'];

    $logo = get_template_directory_uri() . '/images/logo.png';
    //    $grecaptcha = $_POST['g-recaptcha-response'];

    //    $google_options = get_option('hlp_google_settings');

    //    if ($grecaptcha) {
    //        $post_data = http_build_query(
    //            array(
    //                'secret' => $google_options['secretkey'],
    //                'response' => $grecaptcha,
    //                'remoteip' => $_SERVER['REMOTE_ADDR']
    //            ), '', '&');
    //
    //        $opts = array('http' =>
    //                      array(
    //                          'method'  => 'POST',
    //                          'header'  => 'Content-type: application/x-www-form-urlencoded',
    //                          'content' => $post_data
    //                      )
    //                     );
    //
    //        $context  = stream_context_create($opts);
    //        $response = file_get_contents('https://www.google.com/recaptcha/api/siteverify', false, $context);
    //        $captcha_response = json_decode($response);
    //    }
    //
    //    if($captcha_response->success == true) {
    ob_start();
    require_once get_theme_file_path( '/templates/contact-form-email.php' );
    $body = ob_get_clean();
    $body = str_replace( [
        '{fullname}',
        '{email}',
        '{phone}',
        '{company_name}',
        '{subject}',
        '{message}',
        '{logo}'
    ], [
        $full_name,
        $email,
        $phone,
        $company_name,
        $subejct,
        $message,
        $logo
    ], $body );

    $path = ABSPATH . WPINC . '/class-phpmailer.php';

    if (file_exists($path)) {
        require_once($path);
        $mail = new PHPMailer;
    } else {
        require_once ABSPATH . WPINC . '/PHPMailer/PHPMailer.php';
        $mail = new PHPMailer\PHPMailer\PHPMailer;
    }

    $mail->isHTML( true );
    $mail->Body = $body;
    $mail->CharSet = 'UTF-8';
    $mail->addAddress( 'ochoa.robert1@gmail.com' );
    //    $mail->addAddress( 'nefrain@gmail.com' );
    $mail->setFrom( "noreply@{$_SERVER['SERVER_NAME']}", esc_html( get_bloginfo( 'name' ) ) );
    $mail->Subject = esc_html__( 'Tisserie: New Message Request', 'tisserie' );

    if ( ! $mail->send() ) {
        wp_send_json_error( esc_html__( 'Your request could not be sent. Please try again.', 'tisserie' ), 400 );
    } else {
        wp_send_json_success( esc_html__( "Thank You for your interest in our products, you will be contacted shortly.", 'tisserie' ), 200 );
    }

    //    } else {
    //        wp_send_json_error( esc_html__( 'Your request2 could not be sent. Please try again.', 'holpack' ), 400 );
    //    }

    wp_die();
}

/* --------------------------------------------------------------
    ADD OVERLAY GALLERY IMAGE
-------------------------------------------------------------- */
add_action('wp_ajax_nopriv_gallery_wrapper', 'gallery_wrapper_handler');
add_action('wp_ajax_gallery_wrapper', 'gallery_wrapper_handler');

function gallery_wrapper_handler() {
    if ( defined( 'WP_DEBUG' ) && WP_DEBUG ) {
        error_reporting( E_ALL );
        ini_set( 'display_errors', 1 );
    }

    $imgID = $_POST['imgID'];
    $position = $_POST['position'];
    $postID = $_POST['postID'];

    $args = array('post_type' => 'product', 'posts_per_page' => -1, 'order' => 'ASC', 'orderby' => 'date');
    $arr_products = new WP_Query($args);

    $quantity = $arr_products->found_posts;

    $bg_banner = wp_get_attachment_image_src($imgID, 'full', false);
    ob_start();
?>
<div class="gallery-overlay-item">
    <div class="gallery-close"></div>
    <div class="gallery-overlay-image-wrapper">
        <img itemprop="image" content="<?php echo $bg_banner[0];?>" src="<?php echo $bg_banner[0];?>" title="<?php echo get_post_meta( $imgID, '_wp_attachment_image_alt', true ); ?>" alt="<?php echo get_post_meta($imgID, '_wp_attachment_image_alt', true ); ?>" class="img-fluid" width="<?php echo $bg_banner[1]; ?>" height="<?php echo $bg_banner[2]; ?>" />
        <?php $posted = get_post($postID); ?>
        <h3><?php echo $posted->post_title; ?></h3>
    </div>

    <?php $next = $position + 1; ?>
    <?php $prev = $position - 1; ?>

    <div class="gallery-prev" data-position="<?php echo $prev; ?>">
        <i class="fa fa-chevron-left"></i>
    </div>
    <div class="gallery-next" data-position="<?php echo $next; ?>">
        <i class="fa fa-chevron-right"></i>
    </div>
</div>
<?php
    $content = ob_get_clean();
    wp_send_json_success( $content, 200 );

    wp_die();
}
